<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/zd/delzd.do?zd.zdid="+xid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>基础数据</a>&nbsp;-&nbsp;<a>产品列表</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
						
						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px" class="tdColor">名称</td>
							<td width="266px" class="tdColor">型号</td>
							<td width="166px" class="tdColor">等级/批次</td>
							<td width="166px" class="tdColor">单位</td>
							<td width="166px" class="tdColor">单价(元)</td>
							<td width="166px" class="tdColor">备注</td>
						</tr>
						<s:iterator value="#request.cp" id="z">
							<tr height="40px">
								<td><s:property value="cpid" /></td>
								<td><s:property value="cpname" /></td>
								<td><s:property value="cpxinghao" /></td>
								<td><s:property value="dengjipici" /></td>						
								<td><s:property value="danwei" /></td>
								<td><s:property value="danjia" /></td>
								<td><s:property value="beizhu" /></td>
							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>