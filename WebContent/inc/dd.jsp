<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid,khid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/lxr/dellxr.do?lxr.lid="+xid+"&lxr.khid="+khid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户管理</a>&nbsp;-&nbsp;<a
					href="${ctx }/khxxgl/findkhxx.do">客户信息管理</a>&nbsp;-&nbsp;<a>订单历史</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<span><a
								href="${ctx }/lxr/gotolxradd.do?lxr.khid=${knm.get(0).khid}"
								style="background-color: #fff;"></a></span>

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
<center>
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="" class="tdColor"><b>客户编号</b></td>
							<td width="" class="tdColor"><b>${knm.get(0).khid}</b></td>
							<td></td>
							<td width="" class="tdColor"><b>客户名称</b></td>
							<td width="" class="tdColor"><b>${knm.get(0).khname}</b></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">订单编号</td>
							<td width="166px" class="tdColor">日期</td>
							<td width="216px" class="tdColor">送货地址</td>
							<td width="166px" class="tdColor">状态</td>
							<td width="266px" class="tdColor">操作</td>
						</tr>
						<s:iterator value="#request.dd" id="d">
							<tr height="40px">
								<td><s:property value="ddid" /></td>
								<td><s:property value="riqi" /></td>
								<td><s:property value="dizhi" /></td>
								<td><s:property value="zhuangtai" /></td>
								<td><a href="${ctx}/ddmx/dm.do?ddmx.ddid=${d.ddid}"> 明细
								</a></td>


							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
</center>					
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>

</html>