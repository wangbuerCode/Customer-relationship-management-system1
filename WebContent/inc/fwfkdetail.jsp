<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>

</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>客户服务管理</a>&nbsp;-&nbsp;</span><a>服务反馈处理</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>服务反馈处理</span>
				</div>
<center>			
			<form action="${ctx}/fwgl/fwfk.do?fwgl.fwid=${fwfk.fwid}&fwgl.id=${admin.id}" method="post">
			
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px">${fwfk.fwid }</td>
							<td width="166px" class="tdColor">服务类型</td>
							<td width="266px">${fwfk.fwleixing }</td>
						</tr>
							<tr>
							<td width="166px" class="tdColor">概要</td>
							<td colspan="3">${fwfk.fwgaiyao }</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户编号</td>
							<td width="266px">${fwfk.khid }</td>
							<td width="166px" class="tdColor">状态</td  width="166px">
							<td width="266px">${fwfk.fwzhuangtai }</td>
						</tr>
					
						<tr>
							<td width="166px" class="tdColor">服务请求</td>
							<td width="166px" colspan="3">
								${fwfk.fwqingqiu }
							</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">创建人</td>
							<td>${fwfk.chuangjianrenid }</td>
							<td width="166px" class="tdColor">创建时间</td>
							<td>${fwfk.chuangjianshijian }</td>
					
						</tr>
							<tr>
							<td width="166px" class="tdColor">分配给</td>
							<td>${fwfk.id}</td>
							<td width="166px" class="tdColor">分配时间</td>
							<td>${fwfk.fenpeishijian }</td>
					
						</tr>
						<tr>
							<td width="166px" class="tdColor">服务处理</td>
							<td colspan="3">
								${fwfk.fwchuli }
							</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">处理人</td>
							<td>${fwfk.chuliren }</td>
							<td width="166px" class="tdColor">处理时间</td>
							<td>${fwfk.chulishijian}</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">处理结果</td>
							<td><input type="text" name="fwgl.chulijieguo" size="30" required="required"><span style="color: red">*</span></td>
							<td width="166px" class="tdColor">满意度</td>
							<td>
								<select name="fwgl.manyidu">
									
									<option value="5">✩✩✩✩✩</option>
									<option value="4">✩✩✩✩</option>
									<option value="3">✩✩✩</option>
									<option value="2">✩✩</option>
									<option value="1">✩</option>
								</select>
							
							</td>
						</tr>
					</table>
					<input type="hidden" value="已处理" name="fwgl.fwzhuangtai">
					<div class="bbD">
							<span class="bbDP"> 
								<input type="submit" class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
						</div>
					<div class="paging"></div>
				</div>	
			</form>	
</center>							
			</div>
		</div>
	</div>
</body>
</html>