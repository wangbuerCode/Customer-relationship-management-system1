<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/fwgl/delfw.do?fwgl.fwid="+xid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户服务管理</a>&nbsp;-&nbsp;<a>服务归档列表</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="166px" class="tdColor">客户编号</td>
							<td width="166px" class="tdColor">概要</td>
							<td width="166px" class="tdColor">服务类型</td>
							<td width="166px" class="tdColor">创建人</td>
							<td width="166px" class="tdColor">创建时间</td>
							<td width="166px" class="tdColor">状态</td>					
							<td width="166px" class="tdColor">操作</td>
						</tr>
						<s:iterator value="#request.fwgd" id="x">
							<tr height="40px">

								<td><s:property value="fwid" /></td>
								<td><s:property value="khid" /></td>
								<td><s:property value="fwgaiyao" /></td>
								<td><s:property value="fwleixing" /></td>
								<td><s:property value="chuangjianrenid" /></td>
								<td><s:property value="chuangjianshijian" /></td>	
								<td><s:property value="fwzhuangtai" /></td>			
								<td>
									<a href="fwgl/fwgdList.do?fwgl.fwid=${x.fwid }"> 
										细
									</a>
								</td>


							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>