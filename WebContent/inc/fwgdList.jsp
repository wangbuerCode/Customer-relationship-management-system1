<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>

</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>客户服务管理</a>&nbsp;-&nbsp;</span><a>客户服务详细信息</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>客户服务详细信息</span>
				</div>
<center>			
			
			
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px">${fwgd.fwid }</td>
							<td width="166px" class="tdColor">服务类型</td>
							<td width="266px">${fwgd.fwleixing }</td>
						</tr>
							<tr>
							<td width="166px" class="tdColor">概要</td>
							<td colspan="3">${fwgd.fwgaiyao }</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户编号</td>
							<td width="266px">${fwgd.khid }</td>
							<td width="166px" class="tdColor">状态</td  width="166px">
							<td width="266px">${fwgd.fwzhuangtai }</td>
						</tr>
					
						<tr>
							<td width="166px" class="tdColor">服务请求</td>
							<td width="166px" colspan="3">
								${fwgd.fwqingqiu }
							</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">创建人</td>
							<td>${fwgd.chuangjianrenid }</td>
							<td width="166px" class="tdColor">创建时间</td>
							<td>${fwgd.chuangjianshijian }</td>
					
						</tr>
							<tr>
							<td width="166px" class="tdColor">分配给</td>
							<td>${fwgd.id}</td>
							<td width="166px" class="tdColor">分配时间</td>
							<td>${fwgd.fenpeishijian }</td>
					
						</tr>
						<tr>
							<td width="166px" class="tdColor">服务处理</td>
							<td colspan="3">
								${fwgd.fwchuli }
							</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">处理人</td>
							<td>${fwgd.chuliren }</td>
							<td width="166px" class="tdColor">处理时间</td>
							<td>${fwgd.chulishijian}</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">处理结果</td>
							<td>${fwgd.chulijieguo }</td>
							<td width="166px" class="tdColor">满意度</td>
							<td>							
								${fwgd.manyidu }
							</td>
						</tr>
					</table>
					
					<div class="paging"></div>
				</div>	
			
</center>							
			</div>
		</div>
	</div>
</body>
</html>