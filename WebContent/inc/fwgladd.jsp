<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>

</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>客户服务管理</a>&nbsp;-&nbsp;</span><a>服务创建</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>创建客户服务</span>
				</div>
<center>			
			<form action="${ctx}/fwgl/add.do" method="post">
			
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px"><input readonly="true" type="text" name="fwgl.fwid" size="30"><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">服务类型</td>
							<td width="266px">
								<select  name="fwgl.fwleixing">
								<s:iterator value="#request.zd" id="a">
									<option><s:property value="tiaomu" /></option>
								</s:iterator>
									
								</select>
							<span style="color: red">*</span></td>
						</tr>
							<tr>
							<td width="166px" class="tdColor">概要</td>
							<td colspan="3"><input type="text" name="fwgl.fwgaiyao" size="90" style="padding-right:4px;" required="required"><span style="color: red">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户编号</td>
							<td width="266px"><input type="text" name="fwgl.khid" size="30" required="required"><span style="color: red">*</span></td>
							<td width="166px" class="tdColor">状态</td  width="166px">
							<td width="266px"><input type="text" readonly="true" name="fwgl.fwzhuangtai" value="新创建" size="30"><span style="color: #fff">*</span></td>
						</tr>
					
						<tr>
							<td width="166px" class="tdColor">服务请求</td>
							<td width="166px" colspan="3">
								<textarea rows="5" cols="90" style="margin-top:4px;" name="fwgl.fwqingqiu" required="required" ></textarea><span style="color: red">*</span>
							</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">创建人</td>
							<td><input readonly="true" value="${admin.id}" type="text" name="fwgl.chuangjianrenid" size="30" required="required"><span style="color: red">*</span></td>
							<td width="166px" class="tdColor">创建时间</td>
							<td><input readonly="true" type="text" value="<%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())%>" name="fwgl.chuangjianshijian" size="30" required="required"><span style="color: red">*</span></td>
					
						</tr>

					</table>
						<div class="bbD">
							<span class="bbDP"> 
								<input type="submit" class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
						</div>
					
					<div class="paging"></div>
				</div>	
			</form>	
</center>							
			</div>
		</div>
	</div>
</body>
</html>