<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="${ctx }/assets/images/favicon.png">
    <title>部门管理</title>
    <!-- Bootstrap Core CSS -->
    <link href="${ctx }/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="${ctx }/inc/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="${ctx }/inc/css/colors/blue.css" id="theme" rel="stylesheet">

</head>

<body class="fix-header fix-sidebar card-no-border" style="overflow:hidden;">
            <div class="container-fluid">
             	<div style="height:2px;"></div>
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card" style="width:100%;">
                            <div class="card-block" >
                              
                                <div style="text-align:center;" class="table-responsive">
                                  	<img src="${ctx }/images/a.jpg" width="100%" >
                                  <img src="${ctx }/assets/images/favicon.png">
                                  	<span style="vertical-align: middle;font-size:1.5em;">
                                  		欢迎进入客户关系管理系统<span style="font-size:0.7em;"></span>
                                  	</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
   <script src="${ctx }/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="${ctx }/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="${ctx }/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="${ctx }/inc/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="${ctx }/inc/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="${ctx }/inc/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="${ctx }/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="${ctx }/inc/js/custom.min.js"></script>
        <script type="text/javascript" src="${ctx }/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="${ctx }/myplugs/js/plugs.js"></script>

</body>

</html>
