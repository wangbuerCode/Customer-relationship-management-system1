<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">    
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid,khid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/jwjl/deljwjl.do?jwjl.jwid="+xid+"&jwjl.khid="+khid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户管理</a>&nbsp;-&nbsp;<a
					href="${ctx }/khxxgl/findkhxx.do">客户信息管理</a>&nbsp;-&nbsp;<a>交往记录</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<span><a href="${ctx }/jwjl/gotojwjladd.do?jwjl.khid=${knm.get(0).khid}" style="background-color:#fff;">新建交往记录</a></span>
							
						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
	<center>
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="" class="tdColor"><b>客户编号</b></td>
							<td width="" class="tdColor"><b>${knm.get(0).khid}</b></td>
							<td></td><td></td>
							<td width="" class="tdColor"><b>客户名称</b></td>
							<td width="" class="tdColor"><b>${knm.get(0).khname}</b></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">时间</td>
							<td width="166px" class="tdColor">地点</td>
							<td width="166px" class="tdColor">概要</td>
							<td width="216px" class="tdColor">详细信息</td>
							<td width="216px" class="tdColor">备注</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
						<s:iterator value="#request.jwjl" id="jw">
						<tr height="40px">						 
							<td><s:property value="jwshijian" /></td>
							<td><s:property value="jwdidian" /></td>
							<td><s:property value="gaiyao" /></td>
							<td><s:property value="xiangxixinxi" /></td>
							<td><s:property value="beizhu" /></td>
							<td>
								<a href="${ctx}/jwjl/detailjwjl.do?jwjl.jwid=${jw.jwid}">
									<img class="operation" src="img/update.png">
								</a> 
								<a onclick="del('${jw.jwid}','${jw.khid }')">
									<img class="operation" src="img/delete.png">
								</a>
							</td>
								
						
						</tr>
						</s:iterator>		
					</table>
					
</center>					
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>

</html>