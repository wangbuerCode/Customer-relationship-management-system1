<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>客户管理</a>&nbsp;-</span>&nbsp;<a
					href="${ctx }/khxxgl/findkhxx.do">客户信息管理</a>&nbsp;-&nbsp;<a>交往记录</a>&nbsp;-&nbsp;<a>编辑交往记录</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>编辑交往记录</span>
				</div>
				<form action="${ctx}/jwjl/jwjlupdate.do" method="post">
					<div class="baBody">
						<div class="bbD">
							<input name="jwjl.jwid" value="${requestScope.jwjl.jwid }"
								type="hidden"> <input name="jwjl.khid"
								value="${requestScope.jwjl.khid}" type="hidden"> 时间：<input
								value="${requestScope.jwjl.jwshijian}" name="jwjl.jwshijian"
								type="text" class="input3" required="required" /> <span
								style="color: red">*</span> 地点：<input
								value="${requestScope.jwjl.jwdidian}" name="jwjl.jwdidian"
								type="text" class="input3" required="required" /> <span
								style="color: red">*</span>
						</div>
						<div class="bbD">
							概要：<input value="${requestScope.jwjl.gaiyao}" name="jwjl.gaiyao"
								type="text" class="input3" required="required" /> <span
								style="color: red">*</span> 备注：<input
								value="${requestScope.jwjl.beizhu}" name="jwjl.beizhu"
								type="text" class="input3" />
						</div>
						<div class="bbD">
							<span style="float: left">详细信息：</span>
							<textarea rows="5" cols="50" name="jwjl.xiangxixinxi"
								required="required">${requestScope.jwjl.xiangxixinxi}</textarea>
						</div>
						<div class="bbD">
							<span class="bbDP"> <input type="submit"
								class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
</body>
</html>