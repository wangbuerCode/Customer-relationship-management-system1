<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/zd/delzd.do?zd.zdid="+xid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>基础数据</a>&nbsp;-&nbsp;<a>库存信息</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
						
						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px" class="tdColor">产品</td>
							<td width="266px" class="tdColor">仓库</td>
							<td width="166px" class="tdColor">货位</td>
							<td width="166px" class="tdColor">件数</td>
							<td width="166px" class="tdColor">备注</td>
						</tr>
						<s:iterator value="#request.kc" id="z">
							<tr height="40px">
								<td><s:property value="kcid" /></td>
								<td><s:property value="kcchanpin" /></td>
								<td><s:property value="cangku" /></td>
								<td><s:property value="huowei" /></td>						
								<td><s:property value="jianshu" /></td>
								<td><s:property value="beizhu" /></td>
						
							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>