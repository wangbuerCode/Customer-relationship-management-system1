<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/xsjh/delxs.do?xsjh.xid="+xid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>销售管理</a> -&nbsp;</span><a>客户开发计划</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">		
						<div class="cfD">
							<div class="baTopNo">
								客户开发计划
							</div>

						</div>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="166px" class="tdColor">客户名称</td>
							<td width="266px" class="tdColor">概要</td>
							<td width="166px" class="tdColor">联系人</td>
							<td width="166px" class="tdColor">联系人电话</td>
							<td width="166px" class="tdColor">创建时间</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
						<s:iterator value="#request.xsjh" id="xs">
							<tr height="40px">
								<td><s:property value="xid" /></td>
								<td><s:property value="kehumingcheng" /></td>
								<td><s:property value="gaiyao" /></td>
								<td><s:property value="lianxiren" /></td>
								<td><s:property value="lianxirendh" /></td>
								<td><s:property value="chuangjianshijian" /></td>
								<td><a href="${ctx}/xsjh/zd.do?xsjh.xid=${xs.xid}">制定</a>&nbsp|&nbsp<a
									href="${ctx}/xsjh/zx.do?xsjh.xid=${xs.xid}">执行</a></td>
							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>