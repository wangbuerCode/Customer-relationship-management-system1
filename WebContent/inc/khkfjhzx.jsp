<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>销售管理</a>&nbsp;-&nbsp;<a
					href="${ctx }/xsjh/findkf.do?xsjh.zhipairen=${admin.id}">客户开发计划</a>&nbsp;-</span>&nbsp;<a>执行销售计划</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>执行销售计划</span>
				</div>

				<div class="baBody">
<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px"><input name="xsjh.xid" value="${requestScope.xsjh.xid}"
							type="text" class="input3" readonly="true" /><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">机会来源</td>
							<td width="266px"><input
							name="xsjh.jihuilaiyuan" readonly="true"
							value="${requestScope.xsjh.jihuilaiyuan}" type="text"
							class="input3" /><span style="color: #fff">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户名称</td>
							<td width="266px"><input name="xsjh.kehumingcheng"
							readonly="true" value="${requestScope.xsjh.kehumingcheng}"
							type="text" class="input3" required="required" /><span
							style="color: red">*</span></td>
							<td width="166px" class="tdColor">成功几率</td>
							<td width="266px"><input name="xsjh.chenggongjilv" readonly="true"
							value="${requestScope.xsjh.chenggongjilv}" type="text"
							class="input3" required="required" /><span style="color: red">*</span></td>
						</tr>	
						<tr>
							<td width="166px" class="tdColor">联系人</td>
							<td width="266px"><input name="xsjh.lianxiren" readonly="true"
							value="${requestScope.xsjh.lianxiren}" type="text" class="input3" /><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">联系人电话</td>
							<td width="266px">	<input name="xsjh.lianxirendh" readonly="true"
							value="${requestScope.xsjh.lianxirendh}" type="text"
							class="input3" /><span style="color: #fff">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">概要</td>
							<td width="266px" colspan="3"><input name="xsjh.gaiyao" readonly="true" value="${requestScope.xsjh.gaiyao}" style="width: 591px;" type="text" class="input3" required="required" /><span style="color: red">*</span></td>
							
						</tr>
						<tr>
							<td width="166px" class="tdColor">机会描述</td>
							<td width="266px" colspan="3"><textarea rows="5" cols="50" readonly="true" style="margin-top:4px;"
							name="xsjh.jihuimiaoshu" required="required">${requestScope.xsjh.jihuimiaoshu}</textarea><span style="color: red">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">创建人</td>
							<td width="266px"><input name="xsjh.chuangjinren"
							value="${requestScope.xsjh.chuangjinren}" type="text"
							class="input3" readonly="true" /><span style="color: red">*</span></td>
							<td width="166px" class="tdColor">创建时间</td>
							<td width="266px"><input name="xsjh.chuangjianshijian" type="text"
							class="input3" value="${requestScope.xsjh.chuangjianshijian}"
							readonly="true" /><span style="color: red">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">指派给</td>
							<td width="266px"><input type="text" class="input3"
							value="${requestScope.xsjh.zhipairen}" readonly="true"><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">指派时间</td>
							<td width="266px"><input name="xsjh.zhipaishijian" type="text" class="input3"
							value="<%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())%>"
							readonly="true" /><span style="color: #fff">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">日期</td>
							<td width="266px" class="tdColor">计划项</td>
							<td width="266px" colspan="2" class="tdColor">执行效果</td>
						</tr>
						<s:iterator value="#request.khkfjh" id="s">	
							<form action="${ctx}/khkfjh/zxxg.do" method="post">
							<tr>
								<td width="166px"  ><s:property value="date" /></td>
								<td width="266px" ><s:property value="jihuaxiang" /></td>
								<td><input value="<s:property value="zhixingxiaoguo" />" type="text" name="khkfjh.zhixingxiaoguo" class="input3"></td>
								<td><input type="submit" value="保存"></td>
							</tr>
								<input type="hidden" name="khkfjh.kid" value="<s:property value="kid" />">
								 <input type="hidden" name="khkfjh.xid" value="${requestScope.xsjh.xid}"> 
								<input value="<s:property value="date" />" name="khkfjh.date" type="hidden" class="input3"> 
								<input value="<s:property value="jihuaxiang" />" name="khkfjh.jihuaxiang" type="hidden" class="input3">
							</form>	
						</s:iterator>
					</table>
</div>

				</div>

			</div>
		</div>
	</div>
</body>
</html>