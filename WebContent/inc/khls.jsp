<%@ page language="java" contentType="text/html; charset=utf-8" 
	pageEncoding="utf-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/khxxgl/delkhxx.do?khxx.khid="+xid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户管理</a>&nbsp;-&nbsp;<a>客户流失管理</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<a href="${ctx }/inc/khxxgladd.jsp"
								style="background-color: #fff;"> </a>

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="166px" class="tdColor">客户</td>
							<td width="266px" class="tdColor">客户经理</td>
							<td width="166px" class="tdColor">上次下单时间</td>
							<td width="166px" class="tdColor">确认流失时间</td>
							<td width="166px" class="tdColor">状态</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
							<c:forEach items="${requestScope.khls}" var="z">
							<c:if test="${z.cha > '180'}">
							<tr height="40px">
							
								<td>${z.lsid }</td>
								<td>${z.khid }</td>
								<td>${z.id }</td>
								<td>${z.shangcixiadansj }</td>
								<td>${z.quedingliushisj }</td>
								<td>${z.liushizhuangtai }</td>

								<td>
									<c:if test="${z.liushizhuangtai != '已流失'}">
									<a
									href="${ctx}/khls/zanhuanliushi.do?khls.lsid=${z.lsid}">
										暂缓流失 </a> <a
									href="${ctx}/khls/querenliushi.do?khls.lsid=${z.lsid}">
										确认流失 </a>
									</c:if>	
								</td>
							</tr>
						</c:if>
						</c:forEach>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>

</html>