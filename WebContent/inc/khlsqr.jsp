<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function del(xid) {
		if (confirm("您确认要删除吗？")) {
			window.location.href = "${ctx }/khxxgl/delkhxx.do?khxx.khid=" + xid;
		}
	}
</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户管理</a>&nbsp;-&nbsp;<a
					href="${ctx }/khls/findkhls.do">客户流失管理</a>&nbsp;-&nbsp;<a>确认流失</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<a href="${ctx }/inc/khxxgladd.jsp"
								style="background-color: #fff;"> </a>

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
	<center>			
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="166px">${khls.lsid }</td>
							<td width="166px" class="tdColor">客户</td>
							<td width="166px">${khls.khid }</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户经理</td>
							<td width="166px">${khls.id }</td>
							<td width="166px" class="tdColor">上次下单时间</td  width="166px">
							<td>${khls.shangcixiadansj }</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">暂缓措施</td>
							<td width="166px" colspan="3">${khls.zanhuancuoshi }</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">流失原因</td>
							<td width="166px" colspan="3">${khls.liushiyuanyin }</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">流失原因</td>
							<form action="${ctx}/khls/gengxinlsyy.do?khls.lsid=${khls.lsid }" method="post">
								<input type="hidden" value="已流失" name="khls.liushizhuangtai">
								<input value="<%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())%>"
										type="hidden" name="khls.quedingliushisj">
								<td colspan="2">
									<textarea style="margin-top:4px;" rows="5" cols="25" name="khls.liushiyuanyin" required="required"></textarea>
								</td>
								<td><input type="submit" value="确认"></td>
							</form>
						</tr>

					</table>
					<div class="paging"></div>
	</center>				
				</div>				
			</div>
		</div>
	</div>
</body>

</html>