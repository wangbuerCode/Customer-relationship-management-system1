<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/khxxgl/delkhxx.do?khxx.khid="+xid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户管理</a>&nbsp;-&nbsp;<a>客户信息管理</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<a href="${ctx }/khxxgl/gotoadd.do"
								style="background-color: #fff;">新建客户信息</a>

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">客户编号</td>
							<td width="266px" class="tdColor">客户名称</td>
							<td width="166px" class="tdColor">地区</td>
							<td width="166px" class="tdColor">客户经理</td>
							<td width="166px" class="tdColor">邮政编码</td>
							<td width="166px" class="tdColor">电话</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
						<s:iterator value="#request.khxx" id="kh">
							<tr height="40px">

								<td><s:property value="khid" /></td>
								<td><s:property value="khname" /></td>
								<td><s:property value="khdiqu" /></td>
								<td><s:property value="khjingli" /></td>
								<td><s:property value="youbian" /></td>
								<td><s:property value="dianhua" /></td>

								<td><a
									href="${ctx}/khxxgl/detailkhxx.do?khxx.khid=${kh.khid}"> <img
										class="operation" src="img/update.png">
								</a> <a onclick="del('${kh.khid}')"> <img class="operation"
										src="img/delete.png">
								</a></td>


							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>