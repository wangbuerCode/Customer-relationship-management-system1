<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>客户管理</a>&nbsp;-</span>&nbsp;<a
					href="${ctx }/khxxgl/findkhxx.do">客户信息管理</a>&nbsp;-&nbsp;<a>新建客户信息</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>新建客户信息</span>
				</div>
				
				<center>				
					<form action="${ctx}/khxxgl/khxxadd.do" method="post">
					<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">客户编号</td>
							<td width="266px"><input name="khxx.khid" type="text" readonly="true" class="input3" /><span style="color:#fff">*</span></td>
							<td width="166px" class="tdColor">客户名称</td>
							<td width="266px"><input name="khxx.khname" type="text"
								class="input3" required="required" /><span style="color: red">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">地区</td>
							<td width="266px"><input name="khxx.khdiqu" type="text" class="input3"
								required="required" /><span style="color: red">*</span>
							</td>
							<td width="166px" class="tdColor">客户经理</td>
							<td width="266px"><input name="khxx.khjingli" type="text" class="input3"
								required="required" /><span style="color: red">*</span> </td>
						</tr>	
						<tr>
							<td width="166px" class="tdColor">客户等级</td>
							<td colspan="1">	
								<select class="input3" name="khxx.khdengji">
								<s:iterator value="#request.zd" id="a">
									<option><s:property value="tiaomu" /></option>
								</s:iterator>
								
									
								</select><span style="color: red">*</span>
							</td>
							<td width="166px" class="tdColor"></td>
							<td></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户满意度</td>
							<td width="266px"><select  name="khxx.khmanyidu">
									<option value="5">✩✩✩✩✩</option>
									<option value="4">✩✩✩✩</option>
									<option value="3">✩✩✩</option>
									<option value="2">✩✩</option>
									<option value="1">✩</option>
								</select><span style="color: red">*</span>
							</td>
							<td width="166px" class="tdColor">客户信用度</td>
							<td width="266px">
								<select name="khxx.khxinyong">
									<option value="5">✩✩✩✩✩</option>
									<option value="4">✩✩✩✩</option>
									<option value="3">✩✩✩</option>
									<option value="2">✩✩</option>
									<option value="1">✩</option>
								</select><span style="color: red">*</span>
							</td>
						</tr>
						<tr><td colspan="4"> <br> </td></tr>
						<tr>
							<td width="166px" class="tdColor">地址</td>
							<td width="266px"><input
								name="khxx.dizhi" type="text" class="input3" required="required" /><span style="color: red">*</span>
							</td>
							<td width="166px" class="tdColor">邮政编码</td>
							<td width="266px">
								<input name="khxx.youbian"
								type="text" class="input3" required="required" /><span
								style="color: red">*</span>
							</td>
							
						</tr>
					
						<tr>
							<td width="166px" class="tdColor">电话</td>
							<td width="266px">
								<input name="khxx.dianhua" type="text" class="input3"
								required="required" /><span style="color: red">*</span> 
							</td>
							<td width="166px" class="tdColor">传真</td>
							<td width="266px">
								<input
								name="khxx.chuanzhen" type="text" class="input3"
								required="required" /><span style="color: red">*</span> 
							</td>
							
						</tr>
						
							<tr>
							<td width="166px" class="tdColor">网址</td>
							<td width="266px">
								<input
								name="khxx.wangzhi" type="text" class="input3"
								required="required" /><span style="color: red">*</span>
							</td>
							<td width="166px" class="tdColor"> </td>
							<td width="266px">
								
							</td>
							
						</tr>
						<tr><td colspan="4"> <br> </td></tr>
						
						<tr>
							<td width="166px" class="tdColor">营业执照注册号</td>
							<td width="266px">
								<input name="khxx.zhucehao" type="text" class="input3" /><span style="color:#fff">*</span>
							</td>
							<td width="166px" class="tdColor">法人</td>
							<td width="266px">
								<input name="khxx.faren" type="text" class="input3"
								required="required" /><span style="color: red">*</span>
							</td>
							
						</tr>
							<tr>
							<td width="166px" class="tdColor">注册资金(万元)</td>
							<td width="266px">
								<input name="khxx.zhucezijin" type="text" class="input3" /><span style="color:#fff">*</span>
							</td>
							<td width="166px" class="tdColor">年营业额</td>
							<td width="266px">
								<input name="khxx.nianyingyee" type="text" class="input3" /><span style="color:#fff">*</span>
							</td>
							
						</tr>
							<tr>
							<td width="166px" class="tdColor">开户银行</td>
							<td width="266px">
								<input name="khxx.kaihuyinhang" type="text" class="input3"
								required="required" /><span style="color: red">*</span>
							</td>
							<td width="166px" class="tdColor">银行账号</td>
							<td width="266px">
								<input
								name="khxx.yinhangzhanghao" type="text" class="input3"
								required="required" /><span style="color: red">*</span>
							</td>
							
						</tr>
							<tr>
							<td width="166px" class="tdColor">地税登记号</td>
							<td width="266px">
								<input name="khxx.dishuidengjihao" type="text"
								class="input3" /><span style="color:#fff">*</span>
							</td>
							<td width="166px" class="tdColor">国税登记号</td>
							<td width="266px">
								<input name="khxx.guoshuidengjihao"
								type="text" class="input3" /><span style="color:#fff">*</span>
							</td>
							
						</tr>
					
					</table>
						<div class="bbD">
							<span class="bbDP"> <input type="submit"
								class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
						</div>
					<div class="paging"></div>
				</div>	
					<div class="baBody">
					</div>
				</form>
</center>			
				
			</div>

		</div>
	</div>
</body>
</html>