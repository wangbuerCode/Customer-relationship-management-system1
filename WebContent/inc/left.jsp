<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<title>首页左侧导航</title>
<link rel="stylesheet" type="text/css" href="${ctx }/css/public.css" />
<script type="text/javascript" src="${ctx }/js/jquery.min.js"></script>
<script type="text/javascript" src="${ctx }/js/public.js"></script>
<head></head>

<body id="bg">
	<!-- 左边节点 -->
	<div class="container">

		<div class="leftsidebar_box" style="background-color:fffdfd">
			<a href="${ctx }/inc/index.jsp" target="index"><div class="line">
					<img src="${ctx }/img/coin01.png" />&nbsp;&nbsp;首页
				</div></a>
			<!-- <dl class="system_log">
			<dt><img class="icon1" src="${ctx }/img/coin01.png" /><img class="icon2"src="${ctx }/img/coin02.png" />
				首页<img class="icon3" src="${ctx }/img/coin19.png" /><img class="icon4" src="${ctx }/img/coin20.png" /></dt>
		</dl> -->
			<dl class="system_log">
				<dt>
					<img class="icon1" src="${ctx }/img/coin03.png" /><img
						class="icon2" src="${ctx }/img/coin04.png" /> 销售管理<img
						class="icon3" src="${ctx }/img/coin19.png" /><img class="icon4"
						src="${ctx }/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/xsjh/findxsjh.do" target="index">销售机会管理</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/xsjh/findkf.do?xsjh.zhipairen=${admin.id}" target="index">客户开发计划</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
			</dl>

			<dl class="system_log">
				<dt>
					<img class="icon1" src="${ctx }/img/coin07.png" /><img
						class="icon2" src="${ctx }/img/coin08.png" /> 客户管理<img
						class="icon3" src="${ctx }/img/coin19.png" /><img class="icon4"
						src="${ctx }/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/khxxgl/findkhxx.do" target="index" class="cks">客户信息管理</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/khls/findkhls.do" target="index" class="cks">客户流失管理</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
			</dl>

			<dl class="system_log">
				<dt>
					<img class="icon1" src="${ctx }/img/coin05.png" /><img
						class="icon2" src="${ctx }/img/coin06.png" /> 服务管理<img
						class="icon3" src="${ctx }/img/coin19.png" /><img class="icon4"
						src="${ctx }/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/fwgl/gotoadd.do" target="index">服务创建</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/fwgl/findfwgl.do" target="index">服务分配</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
					<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/fwgl/findfwcl.do?fwgl.id=${admin.id}" target="index">服务处理</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/fwgl/findfwfk.do?fwgl.id=${admin.id}" target="index">服务反馈</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a class="cks"
						href="${ctx }/fwgl/findfwgd.do?fwgl.id=${admin.id}" target="index">服务归档</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
			</dl>



			<dl class="system_log">
				<dt>
					<img class="icon1" src="${ctx }/img/coin17.png" /><img
						class="icon2" src="${ctx }/img/coin18.png" />统计报表<img
						class="icon3" src="${ctx }/img/coin19.png" /><img class="icon4"
						src="${ctx }/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/tjbb/gxfx.do" target="index" class="cks">客户贡献分析</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/tjbb/gcfx.do" target="index" class="cks">客户构成分析</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/tjbb/fwfx.do" target="index" class="cks">客户服务分析</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/tjbb/lsfx.do" target="index" class="cks">客户流失分析</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="${ctx }/img/coinL1.png" /><img
						class="icon2" src="${ctx }/img/coinL2.png" /> 基础数据<img
						class="icon3" src="${ctx }/img/coin19.png" /><img class="icon4"
						src="${ctx }/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/zd/zdList.do" target="index" class="cks">数据字典管理</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
					<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/cp/cpList.do" target="index" class="cks">查询产品信息</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
					<dd>
					<img class="coin11" src="${ctx }/img/coin111.png" /><img
						class="coin22" src="${ctx }/img/coin222.png" /><a
						href="${ctx }/kc/kcList.do" target="index" class="cks">查询库存</a><img
						class="icon5" src="${ctx }/img/coin21.png" />
				</dd>
			
			</dl>

		</div>

	</div>
</body>
</html>
