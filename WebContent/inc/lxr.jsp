<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid,khid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/lxr/dellxr.do?lxr.lid="+xid+"&lxr.khid="+khid;
				}
			}
			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>客户管理</a>&nbsp;-&nbsp;<a
					href="${ctx }/khxxgl/findkhxx.do">客户信息管理</a>&nbsp;-&nbsp;<a>联系人</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<span><a
								href="${ctx }/lxr/gotolxradd.do?lxr.khid=${knm.get(0).khid}"
								style="background-color: #fff;">新建联系人</a></span>

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">

					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="" class="tdColor"><b>客户编号</b></td>
							<td width="" class="tdColor"><b>${knm.get(0).khid}</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td width="" class="tdColor"><b>客户名称</b></td>
							<td width="" class="tdColor"><b>${knm.get(0).khname}</b></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">姓名</td>
							<td width="166px" class="tdColor">性别</td>
							<td width="166px" class="tdColor">职务</td>
							<td width="216px" class="tdColor">办公电话</td>
							<td width="166px" class="tdColor">手机</td>
							<td width="216px" class="tdColor">备注</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
						<s:iterator value="#request.lxr" id="l">
							<tr height="40px">
								<td><s:property value="name" /></td>
								<td><s:property value="sex" /></td>
								<td><s:property value="zhiwei" /></td>
								<td><s:property value="bangongdh" /></td>
								<td><s:property value="dianhua" /></td>
								<td><s:property value="beizhu" /></td>
								<td><a href="${ctx}/lxr/detaillxr.do?lxr.lid=${l.lid}">
										<img class="operation" src="img/update.png">
								</a> <a onclick="del('${l.lid}','${l.khid }')"> <img
										class="operation" src="img/delete.png">
								</a></td>


							</tr>
						</s:iterator>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>