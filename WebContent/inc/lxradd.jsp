<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>客户管理</a>&nbsp;-</span>&nbsp;<a
					href="${ctx }/khxxgl/findkhxx.do">客户信息管理</a>&nbsp;-&nbsp;<a>联系人</a>&nbsp;-&nbsp;<a>新建联系人</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>添加客户信息</span>
				</div>
				<form action="${ctx}/lxr/lxradd.do" method="post">
					<div class="baBody">
						<div class="bbD">
							<input name="lxr.lid" type="hidden"> <input
								name="lxr.khid" value="${requestScope.lxr.khid}" type="hidden">
							姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名：<input
								name="lxr.name" type="text" class="input3" required="required" />
							<span style="color: red">*</span>
							性别：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="lxr.sex" type="radio" checked="checked" value="男" />男&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="lxr.sex" type="radio" value="女" />女
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							职位：<input name="lxr.zhiwei" type="text" class="input3"
								required="required" /> <span style="color: red">*</span>
						</div>
						<div class="bbD">
							办公电话：<input name="lxr.bangongdh" type="text" class="input3"
								required="required" /> <span style="color: red">*</span> 手机：<input
								name="lxr.dianhua" type="text" class="input3" /> 备注：<input
								name="lxr.beizhu" type="text" class="input3" />
						</div>

						<div class="bbD">
							<span class="bbDP"> <input type="submit"
								class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
						</div>
					</div>
				</form>




			</div>

		</div>
	</div>
</body>
</html>