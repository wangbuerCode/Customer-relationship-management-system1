<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>      
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="${ctx }/assets/images/favicon.png">
    <link href="${ctx }/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx }/inc/css/style.css" rel="stylesheet">
    <link href="${ctx }/inc/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	<script type="text/javascript">
		function validate() {
			if (confirm("您确定要退出登录吗?")) {
				return true;
			} else {
				return false;
			}
		}
	</script>
</head>

<body class="fix-header fix-sidebar card-no-border" style="overflow:hidden;">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header" style="width:220px;">
                     <a class="navbar-brand" href="#">
                    	<b style="vertical-align: middle;">
                    	<img src="${ctx }/assets/images/logo-light-icon.png" alt="" class="light-logo" /></b>
                    	<span style="vertical-align: bottom;color:#ffffff;">&nbsp;客户关系管理系统</span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
 
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <span class="nav-link  text-muted" style="margin-right:-14px;">
                            	<span>
                            		<a style="cursor:pointer;color:#fff">欢迎您，${admin.id }</a>&nbsp&nbsp<span style="color:#fff">|</span>&nbsp&nbsp<span style="color:#fff"></span><a style="color:#fff;" href="${ctx }/admin/loginOut.do" target="_parent" onclick="return validate()">退出登录</a>
                            	 </span>             
                            </span>
                            
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
            </div>
   
    <script src="${ctx }/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="${ctx }/assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="${ctx }/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="${ctx }/inc/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="${ctx }/inc/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="${ctx }/inc/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="${ctx }/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="${ctx }/inc/js/custom.min.js"></script>
</body>

</html>
