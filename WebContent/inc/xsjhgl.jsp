<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	
function del(xid){
	if(confirm("您确认要删除吗？")){
		window.location.href =
		"${ctx }/xsjh/delxs.do?xsjh.xid="+xid;
	}
}			
	</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>销售管理</a>&nbsp;-&nbsp;<a>销售机会管理</a>
			</div>
		</div>

		<div class="page">
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<a href="${ctx }/inc/xsjhgladd.jsp"
								style="background-color: #fff;">新建销售信息</a>

						</div>
					</form>
				</div>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="166px" class="tdColor">客户名称</td>
							<td width="266px" class="tdColor">概要</td>
							<td width="166px" class="tdColor">联系人</td>
							<td width="166px" class="tdColor">联系人电话</td>
							<td width="166px" class="tdColor">创建时间</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
						<c:forEach items="${requestScope.xsjh}" var="z">
							<tr height="40px">

								<td>${z.xid }</td>
								<td>${z.kehumingcheng }</td>
								<td>${z.gaiyao }</td>
								<td>${z.lianxiren }</td>
								<td>${z.lianxirendh }</td>
								<td>${z.chuangjianshijian }</td>
<c:if test="${z.zhuangtai != '已指派'}">
								<td>
								
									<a href="${ctx}/xsjh/ass.do?xsjh.xid=${z.xid}" style="background-color: #fff;">
										 <img class="operation" src="img/timg.png">
									</a>
								
									 <a href="${ctx}/xsjh/detailxs.do?xsjh.xid=${z.xid}"> <img
										class="operation" src="img/update.png">
									</a> 
									<a onclick="del('${z.xid}')"> <img class="operation"
										src="img/delete.png">
									</a>
								
								</td>
</c:if>

							</tr>
						</c:forEach>
					</table>
					<div class="paging"></div>
				</div>
				<!-- user 表格 显示 end-->
			</div>
			<!-- user页面样式end -->
		</div>

	</div>


</body>
</html>