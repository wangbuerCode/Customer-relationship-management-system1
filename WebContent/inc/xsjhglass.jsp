<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>销售管理</
						a>&nbsp;-&nbsp;<a href="${ctx }/xsjh/findxsjh.do">销售机会管理</a>&nbsp;-</span>&nbsp;<a>指派销售机会</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>指派销售机会</span>
				</div>
			<center>	
				<form action="${ctx}/xsjh/zhipai.do" method="post">
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px"><input name="xsjh.xid" value="${requestScope.xsjh.xid}"
								type="text" class="input3" readonly="true" /><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">机会来源</td>
							<td width="266px"><input
								name="xsjh.jihuilaiyuan"
								value="${requestScope.xsjh.jihuilaiyuan}" type="text"
								class="input3" /><span style="color: #fff">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">客户名称</td>
							<td width="266px"><input name="xsjh.kehumingcheng"
								value="${requestScope.xsjh.kehumingcheng}" type="text"
								class="input3" required="required" /><span style="color: red">*</span></td>
							<td width="166px" class="tdColor">成功几率</td>
							<td width="266px"><input name="xsjh.chenggongjilv"
								value="${requestScope.xsjh.chenggongjilv}" type="text"
								class="input3" required="required" /><span style="color: red">*</span></td>
						</tr>	
						<tr>
							<td width="166px" class="tdColor">概要</td>
							<td colspan="3"><input style="width:591px;" name="xsjh.gaiyao" value="${requestScope.xsjh.gaiyao}"
								style="width: 600px;" type="text" class="input3"
								required="required" /><span style="color: red">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">联系人</td>
							<td width="266px"><input name="xsjh.lianxiren"
								value="${requestScope.xsjh.lianxiren}" type="text"
								class="input3" /><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">联系人电话</td>
							<td width="266px">	<input name="xsjh.lianxirendh"
								value="${requestScope.xsjh.lianxirendh}" type="text"
								class="input3" /><span style="color: #fff">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">机会描述</td>
							<td colspan="3">
								<textarea style="margin-top:4px;" rows="5" cols="50" name="xsjh.jihuimiaoshu"
								required="required">${requestScope.xsjh.jihuimiaoshu}</textarea><span style="color: red">*</span>
							</td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">创建人</td>
							<td width="266px"><input name="xsjh.chuangjinren"
								value="${requestScope.xsjh.chuangjinren}" type="text"
								class="input3" readonly="true" /><span style="color: red">*</span></td>
							<td width="166px" class="tdColor">创建时间</td>
							<td width="266px"><input name="xsjh.chuangjianshijian" type="text"
								class="input3" value="${requestScope.xsjh.chuangjianshijian}"
								readonly="true" /><span style="color: red">*</span></td>
						</tr>
						<tr>
							<td width="166px" class="tdColor">指派给</td>
							<td width="266px">
								 <select name="xsjh.zhipairen"><s:iterator
									value="#request.admin" id="a">
									<option><s:property value="id" /></option>
								</s:iterator></select> 
							</td>
							<td width="166px" class="tdColor">指派时间</td>
							<td width="266px"><input name="xsjh.zhipaishijian" type="text"
								class="input3"
								value="<%=new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date())%>"
								readonly="true" /><span style="color: #fff">*</span></td>
						</tr>
					</table>
					 <input name="xsjh.zhuangtai"
								value="已指派" type="hidden">
						
							<div class="bbD">
							<span class="bbDP"> 
								<input type="submit" class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
							</div>
					<div class="paging"></div>
				</div>	
				
					

					</div>
				</form>
		</center>		
			</div>
		</div>
	</div>
</body>
</html>