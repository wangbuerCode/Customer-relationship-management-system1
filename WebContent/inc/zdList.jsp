<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
		
			function del(xid){
				if(confirm("您确认要删除吗？")){
					window.location.href =
					"${ctx }/zd/delzd.do?zd.zdid="+xid;
				}
			}
			
</script>

</head>

<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;</span><a>基础数据</a>&nbsp;-&nbsp;<a>数据字典管理</a>
			</div>
		</div>

		<div class="page">
		
			<!-- user页面样式 -->
			<div class="connoisseur">
				<div class="conform">
					<form>
						<div class="cfD">
							<a href="${ctx }/inc/zdadd.jsp"
								style="background-color: #fff;">新建数据字典条目</a>

						</div>
					</form>
				</div>
				<center>
				<!-- user 表格 显示 -->
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px" class="tdColor">类别</td>
							<td width="266px" class="tdColor">条目</td>
							<td width="166px" class="tdColor">值</td>
							<td width="166px" class="tdColor">是否可编辑</td>
							<td width="166px" class="tdColor">操作</td>
						</tr>
						
						<c:forEach items="${requestScope.zd}" var="z">
							<tr height="40px">
								<td>${z.zdid }</td>
								<td>${z.leibie }</td>
								<td>${z.tiaomu }</td>
								<td>${z.zhi }</td>						
								<td>${z.tf }</td>
								
								<td>								
									<c:if test="${z.tf == '是'}">
									
										<a href="${ctx}/zd/detailzd.do?zd.zdid=${z.zdid}"> 
											<img class="operation" src="img/update.png">
										</a> 
										<a onclick="del('${z.zdid}')"> 
											<img class="operation" src="img/delete.png">
										</a>
									</c:if>
								</td>
							</tr>
						</c:forEach>	
						
					</table>
					<div class="paging"></div>
				</div>
					</center>		
			</div>
	
		</div>
	</div>
</body>
</html>