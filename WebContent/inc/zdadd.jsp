<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>

</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>基础数据</a>&nbsp;-&nbsp;</span><a href="${ctx }/zd/zdList.do">数据字典管理</a>&nbsp;-&nbsp;</span><a>新建数据字典条目</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>新建数据字典条目</span>
				</div>
<center>			
			<form action="${ctx}/zd/addzd.do" method="post">
			
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px"><input readonly="true" type="text" name="zd.zdid" size="30"><span style="color: #fff">*</span></td>
							<td width="166px" class="tdColor">类别</td>
							<td width="266px"><input type="text" name="zd.leibie" size="30"><span style="color: red">*</span></td>
						
						</tr>
						<tr>	
							<td width="166px" class="tdColor">条目</td>
							<td width="266px"><input type="text" name="zd.tiaomu" size="30"><span style="color: red">*</span></td>
							
							<td width="166px" class="tdColor">值</td>
							<td width="266px"><input type="text" name="zd.zhi" size="30"><span style="color: red">*</span></td>
					
						</tr>

						<tr>
							<td width="166px" class="tdColor">是否可编辑</td>
							<td width="166px" colspan="3">
								<select name="zd.tf">
									<option>是</option>
									<option>否</option>
								</select>
							</td>
						</tr>

					</table>
					<input type="submit" value="保存">
					<div class="paging"></div>
				</div>	
			</form>	
</center>							
			</div>
		</div>
	</div>
</body>
</html>