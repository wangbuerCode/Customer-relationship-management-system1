<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.text.SimpleDateFormat,java.util.Date" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/css.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
	function validate() {
		if (confirm("确认信息无误?")) {
			return true;
		} else {
			return false;
		}
	}
</script>

</head>
<body>
	<div id="pageAll">
		<div class="pageTop">
			<div class="page">
				<img src="img/coin02.png" /><span><a
					href="${ctx }/inc/index.jsp">首页</a>&nbsp;-&nbsp;<a>基础数据</a>&nbsp;-&nbsp;</span><a href="${ctx }/zd/zdList.do">数据字典管理</a>&nbsp;-&nbsp;</span><a>编辑数据字典</a>
			</div>
		</div>
		<div class="page ">
			<!-- 上传广告页面样式 -->
			<div class="banneradd bor">
				<div class="baTopNo">
					<span>编辑数据字典</span>
				</div>
<center>			
			<form action="${ctx}/zd/updatezd.do" method="post">
			
				<div class="conShow">
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td width="166px" class="tdColor">编号</td>
							<td width="266px"><input readonly="true" value="${requestScope.zd.zdid}" type="text" name="zd.zdid" size="30"></td>
							<td width="166px" class="tdColor">类别</td>
							<td width="266px">
								<select  name="zd.leibie">
									<option>${requestScope.zd.leibie}</option>
									<option>服务类型</option>
									<option>企业客户等级</option>
								</select><span style="color: red">*</span>	
							</td>
						</tr>
						<tr>	
							<td width="166px" class="tdColor">条目</td>
							<td width="266px">
								<select  name="zd.tiaomu">
									<option>${requestScope.zd.tiaomu}</option>
									<option>战略合作伙伴</option>
									<option>重点开发客户</option>
									<option>合作客户</option>
									<option>大客户</option>
									<option>普通客户</option>
									<option>建议</option>
									<option>咨询</option>
									<option>投诉</option>
								</select><span style="color: red">*</span>
								<td width="166px" class="tdColor">值</td>
								<td width="266px">	<select  name="zd.zhi">
									<option>${requestScope.zd.zhi}</option>
									<option value="5">战略合作伙伴</option>
									<option value="4">重点开发客户</option>
									<option value="3">合作客户</option>
									<option value="2">大客户</option>
									<option value="1">普通客户</option>
									<option value="建议">建议</option>
									<option value="咨询">咨询</option>
									<option value="投诉">投诉</option>
								</select><span style="color: red">*</span></td>
							</td>
						</tr>

						<tr>
							<td width="166px" class="tdColor">是否可编辑</td>
							<td width="166px" colspan="3">
								<select name="zd.tf">
									<option>${requestScope.zd.tf}</option>
									<option>是</option>
									<option>否</option>
								</select>
							</td>
						</tr>

					</table>
								<div class="bbD">
							<span class="bbDP"> 
								<input type="submit" class="btn_ok btn_yes" onclick="return validate()" value="提交">
								<input type="reset" class="btn_ok btn_yes" value="重置">
							</span>
						</div>
						
					<div class="paging"></div>
				</div>	
			</form>	
</center>							
			</div>
		</div>
	</div>
</body>
</html>