package com.crm.action;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.crm.dao.AdminDao;
import com.crm.entity.Admin;
import com.opensymphony.xwork2.ActionSupport;

public class AdminAction extends ActionSupport{
	private Admin ad;
	private AdminDao adao;
	public Admin getAd() {
		return ad;
	}
	public void setAd(Admin ad) {
		this.ad = ad;
	}
	public AdminDao getAdao() {
		return adao;
	}
	public void setAdao(AdminDao adao) {
		this.adao = adao;
	}
	public String login(){
		HttpServletRequest request = ServletActionContext.getRequest();		
		HttpSession session = request.getSession();
		if(adao.Login(ad)){
			session.setAttribute("admin",ad);
			return "index";
		}else{
			request.setAttribute("msg", "<b>用户名密码有误！</b>");
			return "login";
		}
	}
	public String loginOut(){
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpSession session = request.getSession();		
		session.invalidate();		

		String uri = request.getContextPath() + "/";
		Cookie c = new Cookie("id","");
		c.setPath(uri);
		c.setMaxAge(0);
		response.addCookie(c);
		c = new Cookie("pwd","");
		c.setPath(uri);
		c.setMaxAge(0);
		response.addCookie(c);
		return "login";
	}
		
}
