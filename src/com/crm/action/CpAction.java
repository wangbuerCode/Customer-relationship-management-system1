package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.CpDao;
import com.crm.entity.Chanpin;
import com.opensymphony.xwork2.ActionSupport;

public class CpAction extends ActionSupport{
	private Chanpin cp;
	private CpDao cdao;
	public Chanpin getCp() {
		return cp;
	}
	public void setCp(Chanpin cp) {
		this.cp = cp;
	}
	public CpDao getCdao() {
		return cdao;
	}
	public void setCdao(CpDao cdao) {
		this.cdao = cdao;
	}
	public String cpList(){
		List<Chanpin> cp=cdao.cp();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("cp", cp);
		return "cp";
	}
	
}
