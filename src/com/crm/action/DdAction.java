package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.DdDao;
import com.crm.entity.Dingdan;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Lianxiren;
import com.opensymphony.xwork2.ActionSupport;

public class DdAction extends ActionSupport{
	private Dingdan dd;
	private DdDao ddao;
	public Dingdan getDd() {
		return dd;
	}
	public void setDd(Dingdan dd) {
		this.dd = dd;
	}
	public DdDao getDdao() {
		return ddao;
	}
	public void setDdao(DdDao ddao) {
		this.ddao = ddao;
	}
	public String finddd(){
		List<Dingdan> d=ddao.dd(dd);
		List<Kehuxinxi> knm = ddao.khname(dd);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("dd", d);
		req.setAttribute("knm", knm);
		return "dd";
	}
}
