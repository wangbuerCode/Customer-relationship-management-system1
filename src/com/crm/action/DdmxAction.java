package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.DdmxDao;
import com.crm.entity.Dingdan;
import com.crm.entity.Dingdanmingxi;
import com.opensymphony.xwork2.ActionSupport;

public class DdmxAction extends ActionSupport{
	private Dingdanmingxi ddmx;
	private DdmxDao dmdao;
	public Dingdanmingxi getDdmx() {
		return ddmx;
	}
	public void setDdmx(Dingdanmingxi ddmx) {
		this.ddmx = ddmx;
	}
	public DdmxDao getDmdao() {
		return dmdao;
	}
	public void setDmdao(DdmxDao dmdao) {
		this.dmdao = dmdao;
	}
	
	public String dm(){
		List<Dingdan> d=dmdao.dd(ddmx);
		List<Dingdanmingxi> dm=dmdao.ddmx(ddmx);
		
		double zonge = 0;
		for(int i=0;i<dm.size();i++){			
			double jine =Double.parseDouble( dm.get(i).getJine());			
			zonge += jine;
		}
		
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("dd", d);
		req.setAttribute("ddmx", dm);
		req.setAttribute("zonge", zonge);
		return "ddmx";
	}
 }
