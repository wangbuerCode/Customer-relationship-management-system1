package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.FwglDao;
import com.crm.entity.Admin;
import com.crm.entity.Fuwuguanli;
import com.crm.entity.Zidian;
import com.opensymphony.xwork2.ActionSupport;

public class FwglAction extends ActionSupport{
	private Fuwuguanli fwgl;
	private FwglDao fwdao;
	public Fuwuguanli getFwgl() {
		return fwgl;
	}
	public void setFwgl(Fuwuguanli fwgl) {
		this.fwgl = fwgl;
	}
	public FwglDao getFwdao() {
		return fwdao;
	}
	public void setFwdao(FwglDao fwdao) {
		this.fwdao = fwdao;
	}
	public String gotoadd(){
		List<Zidian> d=fwdao.dengji();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("zd", d);
		return "fwgladd";
	}
	public String add(){
		fwdao.save(fwgl);
		return gotoadd();
	}
	
	public String findfwgl(){
		List<Fuwuguanli> fwgl=fwdao.fwglxinxi();
		List<Admin> a=fwdao.adminList();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwgl", fwgl);
		req.setAttribute("admin", a);
		return "fwgl";
	}
	public String delfw(){
		fwdao.delete(fwgl.getFwid());
		return findfwgl();
	}
	public String fenpei(){
		fwdao.fenpei(fwgl);
		return findfwgl();
	}
	public String findfwcl(){
		List<Fuwuguanli> fwcl=fwdao.fwclxinxi(fwgl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwcl", fwcl);	
		return "fwcl";
	}
	public String fwcldetail(){
		Fuwuguanli fwcl=fwdao.fwcl(fwgl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwcl", fwcl);
		return "fwcldetail";
	}
	public String fwcl(){
		fwdao.gengxinfwcl(fwgl);
		return findfwcl();
	}
	public String findfwfk(){
		List<Fuwuguanli> fwfk=fwdao.fwfkxinxi(fwgl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwfk", fwfk);	
		return "fwfk";
	}
	public String fwfkdetail(){
		Fuwuguanli fwfk=fwdao.fwfk(fwgl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwfk", fwfk);
		return "fwfkdetail";
	}
	public String fwfk(){
		int a = Integer.parseInt(fwgl.getManyidu());
		if(a<3){
			fwgl.setFwzhuangtai("已分配");
		}else{
			fwgl.setFwzhuangtai("已归档");
		}
		fwdao.gengxinfwfk(fwgl);
		
		return findfwfk();
	}

	public String findfwgd(){
		List<Fuwuguanli> fwgd=fwdao.fwgdxinxi(fwgl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwgd", fwgd);	
		return "fwgd";
	}
	public String fwgdList(){
		Fuwuguanli fwgd=fwdao.fwgd(fwgl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("fwgd", fwgd);
		return "fwgdList";
	}
	

}
