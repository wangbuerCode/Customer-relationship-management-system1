package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.JwjlDao;
import com.crm.entity.Jiaowangjilu;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Lianxiren;
import com.opensymphony.xwork2.ActionSupport;

public class JwjlAction extends ActionSupport{
	private Jiaowangjilu jwjl;
	private JwjlDao jdao;
	public Jiaowangjilu getJwjl() {
		return jwjl;
	}
	public void setJwjl(Jiaowangjilu jwjl) {
		this.jwjl = jwjl;
	}
	public JwjlDao getJdao() {
		return jdao;
	}
	public void setJdao(JwjlDao jdao) {
		this.jdao = jdao;
	}
	public String findjwjl(){
		List<Jiaowangjilu> j=jdao.jwjl(jwjl);
		List<Kehuxinxi> knm = jdao.khname(jwjl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("jwjl", j);
		req.setAttribute("knm", knm);
		return "jwjl";
	}
	public String deljwjl(){
		jdao.deljwjl(jwjl.getJwid());
		return findjwjl();
	}
	public String gotojwjladd(){
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("jwjl", jwjl);
		return "jwjladd";
	}
	public String jwjladd(){
		jdao.save(jwjl);
		return findjwjl();
	}
	
	public String detailjwjl(){
		Jiaowangjilu j =jdao.detailjwjl(jwjl);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("jwjl", j);
		return "detailjwjl";
	}
	public String jwjlupdate(){
		jdao.updatejwjl(jwjl);
		return findjwjl();
	}

}
