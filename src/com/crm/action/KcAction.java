package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import com.crm.dao.KcDao;
import com.crm.entity.Kucun;
import com.opensymphony.xwork2.ActionSupport;

public class KcAction extends ActionSupport{
	private Kucun kc;
	private KcDao kcdao;
	public Kucun getKc() {
		return kc;
	}
	public void setKc(Kucun kc) {
		this.kc = kc;
	}
	public KcDao getKcdao() {
		return kcdao;
	}
	public void setKcdao(KcDao kcdao) {
		this.kcdao = kcdao;
	}
	public String kcList(){
		List<Kucun> kc=kcdao.kc();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("kc", kc);
		return "kc";
	}
}
