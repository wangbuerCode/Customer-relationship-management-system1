package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.KhkfjhDao;
import com.crm.dao.XsjhDao;
import com.crm.entity.Admin;
import com.crm.entity.Khkfjh;
import com.crm.entity.Xsjh;
import com.opensymphony.xwork2.ActionSupport;

public class KhkfjhAction extends ActionSupport{
	private Khkfjh khkfjh;
	private KhkfjhDao kfdao;

	
	public Khkfjh getKhkfjh() {
		return khkfjh;
	}


	public void setKhkfjh(Khkfjh khkfjh) {
		this.khkfjh = khkfjh;
	}


	public KhkfjhDao getKfdao() {
		return kfdao;
	}


	public void setKfdao(KhkfjhDao kfdao) {
		this.kfdao = kfdao;
	}


	public String kfjh(){
		kfdao.save(khkfjh);
		HttpServletRequest req=ServletActionContext.getRequest();
		
		List<Khkfjh> k = kfdao.kfList(khkfjh);
		req.setAttribute("khkfjh", k);
		
		Xsjh xsjh =kfdao.detailxs(khkfjh);
		req.setAttribute("xsjh", xsjh);

		return "kf";
	}
	
	public String zxxg(){
		HttpServletRequest req=ServletActionContext.getRequest();
		
		kfdao.updatezx(khkfjh);
		
		List<Khkfjh> k = kfdao.kfList(khkfjh);
		req.setAttribute("khkfjh", k);
		
			
		Xsjh xsjh =kfdao.detailxs(khkfjh);
		req.setAttribute("xsjh", xsjh);
		
		return "zx";
	}
	public String update(){
		kfdao.update(khkfjh);
		HttpServletRequest req=ServletActionContext.getRequest();
		
		List<Khkfjh> k = kfdao.kfList(khkfjh);
		req.setAttribute("khkfjh", k);
		
		Xsjh xsjh =kfdao.detailxs(khkfjh);
		req.setAttribute("xsjh", xsjh);
		return "kf";
	}
	public String del(){
		kfdao.del(khkfjh.getKid());
		HttpServletRequest req=ServletActionContext.getRequest();
		
		List<Khkfjh> k = kfdao.kfList(khkfjh);
		req.setAttribute("khkfjh", k);
		
		Xsjh xsjh =kfdao.detailxs(khkfjh);
		req.setAttribute("xsjh", xsjh);
		return "kf";
	}

}
