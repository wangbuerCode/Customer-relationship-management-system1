package com.crm.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.KhlsDao;
import com.crm.entity.Kehuliushi;
import com.opensymphony.xwork2.ActionSupport;

public class KhlsAction extends ActionSupport{
	private Kehuliushi khls;
	private KhlsDao kdao;
	public Kehuliushi getKhls() {
		return khls;
	}
	public void setKhls(Kehuliushi khls) {
		this.khls = khls;
	}
	public KhlsDao getKdao() {
		return kdao;
	}
	public void setKdao(KhlsDao kdao) {
		this.kdao = kdao;
	}
	public String findkhls(){
		List<Kehuliushi> k=kdao.khls();
	
		String d1 = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		String d2 = null;
		for(int i=0;i<k.size();i++){
			int lsid = k.get(i).getLsid();
			d2 = k.get(i).getShangcixiadansj();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				long m = sdf.parse(d1).getTime() - sdf.parse(d2).getTime();
				long x = m/(24*60*60*1000);
				kdao.up(x,lsid);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		List<Kehuliushi> ks=kdao.khls();
		HttpServletRequest request=ServletActionContext.getRequest();
		request.setAttribute("khls", ks);
		return "khls";
	}
	public String zanhuanliushi(){
		HttpServletRequest req=ServletActionContext.getRequest();
		Kehuliushi k =kdao.zanhuanliushi(khls);
		req.setAttribute("khls", k);
		return "zhls";
	}
	public String gengxincuoshi(){
		kdao.gengxincuoshi(khls);
		return zanhuanliushi();
	}
	public String querenliushi(){
		HttpServletRequest req=ServletActionContext.getRequest();
		Kehuliushi k =kdao.zanhuanliushi(khls);
		req.setAttribute("khls", k);
		return "qrls";
	}
	public String gengxinlsyy(){
		kdao.gengxinlsyy(khls);
		return querenliushi();
	}
}
