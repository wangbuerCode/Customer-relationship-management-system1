package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.KhxxDao;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Xsjh;
import com.crm.entity.Zidian;
import com.opensymphony.xwork2.ActionSupport;

public class KhxxAction extends ActionSupport{
	private Kehuxinxi khxx;
	private KhxxDao khdao;
	public Kehuxinxi getKhxx() {
		return khxx;
	}
	public void setKhxx(Kehuxinxi khxx) {
		this.khxx = khxx;
	}
	public KhxxDao getKhdao() {
		return khdao;
	}
	public void setKhdao(KhxxDao khdao) {
		this.khdao = khdao;
	}
	
	public String findkhxx(){
		List<Kehuxinxi> kh=khdao.khxx();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("khxx", kh);
		return "kehuxinxi";
	}
	
	public String delkhxx(){
		khdao.delkhxx(khxx.getKhid());
		return findkhxx();
	}
	
	public String khxxadd(){
		khdao.savekhxx(khxx);
		return findkhxx();
	}
	
	public String detailkhxx(){
		HttpServletRequest req=ServletActionContext.getRequest();
		List<Zidian> d=khdao.dengji();
		req.setAttribute("zd", d);
		
		Kehuxinxi kh =khdao.detailkhxx(khxx);
		req.setAttribute("khxx", kh);
		return "detailkhxx";
	}
	
	public String updatekhxx(){
		khdao.updatekhxx(khxx);
		return findkhxx();
	}
	public String gotoadd(){
		List<Zidian> d=khdao.dengji();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("zd", d);
		return "add";
	}
}
