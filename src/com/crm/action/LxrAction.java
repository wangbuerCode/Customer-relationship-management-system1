package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import com.crm.dao.LxrDao;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Lianxiren;
import com.opensymphony.xwork2.ActionSupport;


public class LxrAction extends ActionSupport{
	private Lianxiren lxr;
	private LxrDao ldao;
	public Lianxiren getLxr() {
		return lxr;
	}
	public void setLxr(Lianxiren lxr) {
		this.lxr = lxr;
	}
	public LxrDao getLdao() {
		return ldao;
	}
	public void setLdao(LxrDao ldao) {
		this.ldao = ldao;
	}
	
	
	public String findlxr(){
		List<Lianxiren> l=ldao.lxr(lxr);
		List<Kehuxinxi> knm = ldao.khname(lxr);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("lxr", l);
		req.setAttribute("knm", knm);
		return "lxr";
	}
	public String dellxr(){
		ldao.dellxr(lxr.getLid());
		return findlxr();
	}
	public String gotolxradd(){
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("lxr", lxr);
		return "lxradd";
	}
	public String lxradd(){
		ldao.save(lxr);
		return findlxr();
	}
	public String detaillxr(){
		Lianxiren l =ldao.detaillxr(lxr);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("lxr", l);
		return "detaillxr";
	}
	public String lxrupdate(){
		ldao.updatelxr(lxr);
		return findlxr();
	}
	
}
