package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.TjbbDao;
import com.crm.entity.Dingdan;
import com.crm.entity.Dingdanmingxi;
import com.crm.entity.Kehuliushi;
import com.crm.entity.Kehuxinxi;
import com.opensymphony.xwork2.ActionSupport;

public class TjbbAction extends ActionSupport{

	private TjbbDao tjdao;
	
	public TjbbDao getTjdao() {
		return tjdao;
	}

	public void setTjdao(TjbbDao tjdao) {
		this.tjdao = tjdao;
	}

	public String gxfx(){
		int a = 0;
		List<Dingdanmingxi> dd = null;
		//查询khid
		List<Kehuxinxi> khxx=tjdao.khxx();
		for(int i=0;i<khxx.size();i++){
			//通过khid 找到ddid
			 a = khxx.get(i).getKhid();
			 
			 dd=tjdao.dingdanList(a);
			 double zonge = 0,jine = 0;
			 for(int k=0;k<dd.size();k++){		
				jine =Double.parseDouble(dd.get(k).getJine());			
				zonge += jine;	
			 }	
			 tjdao.gengxinze(zonge,a);
			 //更新编号
			 tjdao.gengxinbh(i,a);
		}
		//查询更新后的数据
		List<Kehuxinxi> khxxs=tjdao.khxx();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("khxx", khxxs);		
		return "gxfx";
	}
	
	public String gcfx(){
		long a=tjdao.a();
		long b=tjdao.b();
		long c=tjdao.c();
		long d=tjdao.d();
		long e=tjdao.e();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("a", a);	
		req.setAttribute("b", b);
		req.setAttribute("c", c);
		req.setAttribute("d", d);
		req.setAttribute("e", e);
		return "gcfx";
	}
	public String fwfx(){
		long x=tjdao.x();
		long y=tjdao.y();
		long z=tjdao.z();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("x", x);	
		req.setAttribute("y", y);	
		req.setAttribute("z", z);	
		return "fwfx";
	}
	public String lsfx(){
		List<Kehuliushi> khls=tjdao.khls();
		for(int i=0;i<khls.size();i++){
			 int a = khls.get(i).getLsid();
			 tjdao.lsbh(i,a);
		}
		List<Kehuliushi> khlss=tjdao.khls();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("khls", khlss);	
		return "lsfx";
	}
	
}
