package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.XsjhDao;
import com.crm.entity.Admin;
import com.crm.entity.Khkfjh;
import com.crm.entity.Xsjh;
import com.opensymphony.xwork2.ActionSupport;

public class XsjhAction extends ActionSupport{
	private Xsjh xsjh;
	private XsjhDao xdao;
	public Xsjh getXsjh() {
		return xsjh;
	}
	public void setXsjh(Xsjh xsjh) {
		this.xsjh = xsjh;
	}
	public XsjhDao getXdao() {
		return xdao;
	}
	public void setXdao(XsjhDao xdao) {
		this.xdao = xdao;
	}
	//查
	public String findxsjh(){
		List<Xsjh> xsjh=xdao.xsjhxinxi();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("xsjh", xsjh);
		return "xsjh";
	}
	//删
	public String delxs(){
		xdao.delete(xsjh.getXid());
		return findxsjh();
	}
	//增
	public String xsjhgladd(){
		xdao.save(xsjh);
		return findxsjh();
	}
	//查看修改信息页面
	public String detailxs(){
		Xsjh x =xdao.detailxs(xsjh);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("xsjh", x);
		return "detailxs";
	}
	//改
	public String updatexs(){
		xdao.updatexs(xsjh);
		return findxsjh();
	}
	//查看指派页面
	public String ass(){
		Xsjh x =xdao.detailxs(xsjh);
		List<Admin> a=xdao.adminList();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("xsjh", x);
		req.setAttribute("admin", a);
		return "ass";
	}
	//指派
	public String zhipai(){
		return updatexs();
	}

	public String findkf(){
		List<Xsjh> x=xdao.kf(xsjh);
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("xsjh", x);
		return "kf";
	}
	
	public String zd(){
	
		HttpServletRequest req=ServletActionContext.getRequest();
		Xsjh x =xdao.detailxs(xsjh);
		req.setAttribute("xsjh", x);
		
		List<Khkfjh> k = xdao.kfList(xsjh);
		req.setAttribute("khkfjh", k);
		
		return "zd";
	}
	public String zx(){
		
		HttpServletRequest req=ServletActionContext.getRequest();
		Xsjh x =xdao.detailxs(xsjh);
		req.setAttribute("xsjh", x);
		
		List<Khkfjh> k = xdao.kfList(xsjh);
		req.setAttribute("khkfjh", k);
		
		return "zx";
	}
}
