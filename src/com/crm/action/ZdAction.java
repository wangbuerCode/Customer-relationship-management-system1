package com.crm.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.crm.dao.ZdDao;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Zidian;
import com.opensymphony.xwork2.ActionSupport;

public class ZdAction extends ActionSupport{
	private Zidian zd;
	private ZdDao zdao;
	public Zidian getZd() {
		return zd;
	}
	public void setZd(Zidian zd) {
		this.zd = zd;
	}
	public ZdDao getZdao() {
		return zdao;
	}
	public void setZdao(ZdDao zdao) {
		this.zdao = zdao;
	}
	public String zdList(){
		List<Zidian> zd=zdao.zd();
		HttpServletRequest req=ServletActionContext.getRequest();
		req.setAttribute("zd", zd);
		return "zd";
	}
	public String delzd(){
		zdao.delzd(zd.getZdid());
		return zdList();
	}
	public String addzd(){
		zdao.savezd(zd);
		return zdList();
	}
	public String detailzd(){
		HttpServletRequest req=ServletActionContext.getRequest();
		Zidian z =zdao.detailzd(zd);
		req.setAttribute("zd", z);
		return "detailzd";
	}
	public String updatezd(){
		zdao.updatezd(zd);
		return zdList();
	}
}
