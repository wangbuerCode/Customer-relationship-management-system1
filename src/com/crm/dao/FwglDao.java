package com.crm.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Admin;
import com.crm.entity.Fuwuguanli;
import com.crm.entity.Kehuliushi;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Xsjh;
import com.crm.entity.Zidian;

public class FwglDao extends HibernateDaoSupport{

	public List<Fuwuguanli> fwglxinxi()
	{
		return this.getHibernateTemplate().find("from Fuwuguanli where fwzhuangtai='新创建'");
	}
	public void save(Fuwuguanli fwgl)
	{
		this.getHibernateTemplate().save(fwgl);
	}
	public void delete(int xid) {
		String hql="delete from Fuwuguanli x where x.fwid="+xid;
		this.getHibernateTemplate().bulkUpdate(hql);
		
	}
	public List<Admin> adminList(){
		return this.getHibernateTemplate().find("from Admin");
	}
	public void fenpei(Fuwuguanli fwgl){
		String hql="update Fuwuguanli k set k.id='"+fwgl.getId()+"',k.fenpeishijian='"+fwgl.getFenpeishijian()+"',k.fwzhuangtai='"+fwgl.getFwzhuangtai()+"' where k.fwid="+fwgl.getFwid();
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	
	public List<Fuwuguanli> fwclxinxi(Fuwuguanli fwgl)
	{
		return this.getHibernateTemplate().find("from Fuwuguanli where id='"+fwgl.getId()+"' and fwzhuangtai='已分配'");
	}
	public Fuwuguanli fwcl(Fuwuguanli fwgl)
	{
		return (Fuwuguanli)this.getHibernateTemplate().get(Fuwuguanli.class, fwgl.getFwid());
	}
	public void gengxinfwcl(Fuwuguanli fwgl){
		String hql="update Fuwuguanli k set k.fwzhuangtai='"+fwgl.getFwzhuangtai()+"',k.fwchuli='"+fwgl.getFwchuli()+"',k.chuliren='"+fwgl.getChuliren()+"' , k.chulishijian='"+fwgl.getChulishijian()+"' where k.fwid="+fwgl.getFwid();
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	public List<Fuwuguanli> fwfkxinxi(Fuwuguanli fwgl)
	{
		return this.getHibernateTemplate().find("from Fuwuguanli where id='"+fwgl.getId()+"' and fwzhuangtai='已处理'");
	}
	public Fuwuguanli fwfk(Fuwuguanli fwgl)
	{
		return (Fuwuguanli)this.getHibernateTemplate().get(Fuwuguanli.class, fwgl.getFwid());
	}
	public void gengxinfwfk(Fuwuguanli fwgl){
		String hql="update Fuwuguanli k set k.fwzhuangtai='"+fwgl.getFwzhuangtai()+"',k.chulijieguo='"+fwgl.getChulijieguo()+"',k.manyidu='"+fwgl.getManyidu()+"' where k.fwid="+fwgl.getFwid();
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	public List<Fuwuguanli> fwgdxinxi(Fuwuguanli fwgl)
	{
		return this.getHibernateTemplate().find("from Fuwuguanli where id='"+fwgl.getId()+"' and fwzhuangtai='已归档'");
	}
	public Fuwuguanli fwgd(Fuwuguanli fwgl)
	{
		return (Fuwuguanli)this.getHibernateTemplate().get(Fuwuguanli.class, fwgl.getFwid());
	}
	public List<Zidian> dengji(){
		return this.getHibernateTemplate().find("from Zidian where leibie='服务类型'");
	}
}
