package com.crm.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Jiaowangjilu;
import com.crm.entity.Kehuxinxi;
import com.crm.entity.Lianxiren;


public class JwjlDao extends HibernateDaoSupport{

	public List<Jiaowangjilu> jwjl(Jiaowangjilu jwjl){
		return this.getHibernateTemplate().find("from Jiaowangjilu where khid='"+jwjl.getKhid()+"'");
	}
	public List<Kehuxinxi> khname(Jiaowangjilu jwjl){
		return this.getHibernateTemplate().find("from Kehuxinxi where khid='"+jwjl.getKhid()+"'");
	}
	public void deljwjl(int jwid) {
		String hql="delete from Jiaowangjilu x where x.jwid="+jwid;
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	public void save(Jiaowangjilu jwjl)
	{
		this.getHibernateTemplate().save(jwjl);
	}
	public Jiaowangjilu detailjwjl(Jiaowangjilu jwjl)
	{
		return (Jiaowangjilu)this.getHibernateTemplate().get(Jiaowangjilu.class, jwjl.getJwid());
	}
	public void updatejwjl(Jiaowangjilu jwjl)
	{
		this.getHibernateTemplate().update(jwjl);
	}
}
