package com.crm.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Kehuliushi;

public class KhlsDao extends HibernateDaoSupport{
	public List<Kehuliushi> khls(){
		return this.getHibernateTemplate().find("from Kehuliushi");
	}
	public Kehuliushi zanhuanliushi(Kehuliushi khls)
	{
		return (Kehuliushi)this.getHibernateTemplate().get(Kehuliushi.class, khls.getLsid());
	}

	public void gengxincuoshi(Kehuliushi khls){
		String hql="update Kehuliushi k set k.liushizhuangtai='暂缓流失',k.zanhuancuoshi='"+khls.getZanhuancuoshi()+"' where k.lsid="+khls.getLsid();
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	
	public void gengxinlsyy(Kehuliushi khls){
		String hql="update Kehuliushi k set k.liushiyuanyin='"+khls.getLiushiyuanyin()+"',k.quedingliushisj='"+khls.getQuedingliushisj()+"' , k.liushizhuangtai='"+khls.getLiushizhuangtai()+"' where k.lsid="+khls.getLsid();
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	
	public void up(long x,int lsid){
		String hql="update Kehuliushi k set k.cha='"+x+"' where k.lsid='"+lsid+"'";
		this.getHibernateTemplate().bulkUpdate(hql);
	}
}
