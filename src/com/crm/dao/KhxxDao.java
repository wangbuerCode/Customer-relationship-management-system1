package com.crm.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Kehuxinxi;
import com.crm.entity.Zidian;

public class KhxxDao extends HibernateDaoSupport{
	public List<Kehuxinxi> khxx(){
		return this.getHibernateTemplate().find("from Kehuxinxi");
	}
	
	public void delkhxx(int khid) {
		String hql="delete from Kehuxinxi kh where kh.khid="+khid;
		this.getHibernateTemplate().bulkUpdate(hql);
		String hql2="delete from Lianxiren l where l.khid="+khid;
		this.getHibernateTemplate().bulkUpdate(hql2);
		String hql3="delete from Jiaowangjilu x where x.khid="+khid;
		this.getHibernateTemplate().bulkUpdate(hql3);
		
	}
	
	public void savekhxx(Kehuxinxi khxx){
		this.getHibernateTemplate().save(khxx);
	}
	public Kehuxinxi detailkhxx(Kehuxinxi khxx)
	{
		return (Kehuxinxi)this.getHibernateTemplate().get(Kehuxinxi.class, khxx.getKhid());
	}
	public void updatekhxx(Kehuxinxi khxx)
	{
		this.getHibernateTemplate().update(khxx);
	}
	public List<Zidian> dengji(){
		return this.getHibernateTemplate().find("from Zidian where leibie='企业客户等级'");
	}
}
