package com.crm.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Kehuxinxi;
import com.crm.entity.Lianxiren;
import com.crm.entity.Xsjh;

public class LxrDao extends HibernateDaoSupport{
	public List<Lianxiren> lxr(Lianxiren lxr){
		return this.getHibernateTemplate().find("from Lianxiren where khid='"+lxr.getKhid()+"'");
	}
	
	public List<Kehuxinxi> khname(Lianxiren lxr){
		return this.getHibernateTemplate().find("from Kehuxinxi where khid='"+lxr.getKhid()+"'");
	}
	
	public void dellxr(int lid) {
		String hql="delete from Lianxiren x where x.lid="+lid;
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	
	public void save(Lianxiren lxr)
	{
		this.getHibernateTemplate().save(lxr);
	}
	
	public Lianxiren detaillxr(Lianxiren lxr)
	{
		return (Lianxiren)this.getHibernateTemplate().get(Lianxiren.class, lxr.getLid());
	}
	public void updatelxr(Lianxiren lxr)
	{
		this.getHibernateTemplate().update(lxr);
	}
}
