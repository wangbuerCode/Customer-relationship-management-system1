package com.crm.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Admin;
import com.crm.entity.Dingdan;
import com.crm.entity.Dingdanmingxi;
import com.crm.entity.Fuwuguanli;
import com.crm.entity.Kehuliushi;
import com.crm.entity.Kehuxinxi;

public class TjbbDao extends HibernateDaoSupport{
	public List<Kehuxinxi> khxx(){
		return this.getHibernateTemplate().find("from Kehuxinxi");
	}
	
	public List<Dingdanmingxi> dingdanList(int a){
		return this.getHibernateTemplate().find("from Dingdanmingxi where khid="+a);
	}

	public void gengxinbh(int i,int a){
		String hql="update Kehuxinxi k set bianhao='"+(i+1)+"' where k.khid="+a;
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	public void gengxinze(double zonge,int a){
		String hql="update Kehuxinxi k set zonge='"+zonge+"' where k.khid="+a;
		this.getHibernateTemplate().bulkUpdate(hql);
	}
	public long a(){
		String hql = "select count(*) from Kehuxinxi as k where k.khdengji='战略合作伙伴'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long b(){
		String hql = "select count(*) from Kehuxinxi as k where k.khdengji='重点开发客户'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long c(){
		String hql = "select count(*) from Kehuxinxi as k where k.khdengji='合作客户'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long d(){
		String hql = "select count(*) from Kehuxinxi as k where k.khdengji='大客户'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long e(){
		String hql = "select count(*) from Kehuxinxi as k where k.khdengji='普通客户'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long x(){
		String hql = "select count(*) from Fuwuguanli as k where k.fwleixing='建议'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long y(){
		String hql = "select count(*) from Fuwuguanli as k where k.fwleixing='咨询'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	public long z(){
		String hql = "select count(*) from Fuwuguanli as k where k.fwleixing='投诉'";
		  List list=this.getHibernateTemplate().find(hql);
		  long row=0;
		  if(list!=null && list.size()>0)
		  {
			  row=(Long)list.get(0);
		  }
		  return row;
	}
	
	public List<Kehuliushi> khls(){
		return this.getHibernateTemplate().find("from Kehuliushi where liushizhuangtai='已流失'");
	}
	public void lsbh(int i,int a){
		String hql="update Kehuliushi k set bianhao='"+(i+1)+"' where k.lsid="+a;
		this.getHibernateTemplate().bulkUpdate(hql);
	}
}
