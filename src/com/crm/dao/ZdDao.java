package com.crm.dao;

import java.util.List;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.crm.entity.Kehuxinxi;
import com.crm.entity.Zidian;

public class ZdDao extends HibernateDaoSupport{
	public List<Zidian> zd(){
		return this.getHibernateTemplate().find("from Zidian");
	}
	public void delzd(int zdid) {
		String hql="delete from Zidian kh where kh.zdid="+zdid;
		this.getHibernateTemplate().bulkUpdate(hql);
	}	
	public void savezd(Zidian zd){
		this.getHibernateTemplate().save(zd);
	}
	public Zidian detailzd(Zidian zd)
	{
		return (Zidian)this.getHibernateTemplate().get(Zidian.class, zd.getZdid());
	}
	public void updatezd(Zidian zd)
	{
		this.getHibernateTemplate().update(zd);
	}
}
