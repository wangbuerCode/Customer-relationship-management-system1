package com.crm.entity;

/**
 * Chanpin entity. @author MyEclipse Persistence Tools
 */

public class Chanpin implements java.io.Serializable {

	// Fields

	private Integer cpid;
	private String cpname;
	private String cpxinghao;
	private String dengjipici;
	private String danwei;
	private String danjia;
	private String beizhu;

	// Constructors

	/** default constructor */
	public Chanpin() {
	}

	/** full constructor */
	public Chanpin(String cpname, String cpxinghao, String dengjipici,
			String danwei, String danjia, String beizhu) {
		this.cpname = cpname;
		this.cpxinghao = cpxinghao;
		this.dengjipici = dengjipici;
		this.danwei = danwei;
		this.danjia = danjia;
		this.beizhu = beizhu;
	}

	// Property accessors

	public Integer getCpid() {
		return this.cpid;
	}

	public void setCpid(Integer cpid) {
		this.cpid = cpid;
	}

	public String getCpname() {
		return this.cpname;
	}

	public void setCpname(String cpname) {
		this.cpname = cpname;
	}

	public String getCpxinghao() {
		return this.cpxinghao;
	}

	public void setCpxinghao(String cpxinghao) {
		this.cpxinghao = cpxinghao;
	}

	public String getDengjipici() {
		return this.dengjipici;
	}

	public void setDengjipici(String dengjipici) {
		this.dengjipici = dengjipici;
	}

	public String getDanwei() {
		return this.danwei;
	}

	public void setDanwei(String danwei) {
		this.danwei = danwei;
	}

	public String getDanjia() {
		return this.danjia;
	}

	public void setDanjia(String danjia) {
		this.danjia = danjia;
	}

	public String getBeizhu() {
		return this.beizhu;
	}

	public void setBeizhu(String beizhu) {
		this.beizhu = beizhu;
	}

}