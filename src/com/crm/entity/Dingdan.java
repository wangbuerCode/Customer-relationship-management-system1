package com.crm.entity;



/**
 * Dingdan entity. @author MyEclipse Persistence Tools
 */

public class Dingdan  implements java.io.Serializable {


    // Fields    

     private Integer ddid;
     private String khid;
     private String riqi;
     private String dizhi;
     private String zhuangtai;


    // Constructors

    /** default constructor */
    public Dingdan() {
    }

    
    /** full constructor */
    public Dingdan(String khid, String riqi, String dizhi, String zhuangtai) {
        this.khid = khid;
        this.riqi = riqi;
        this.dizhi = dizhi;
        this.zhuangtai = zhuangtai;
    }

   
    // Property accessors

    public Integer getDdid() {
        return this.ddid;
    }
    
    public void setDdid(Integer ddid) {
        this.ddid = ddid;
    }

    public String getKhid() {
        return this.khid;
    }
    
    public void setKhid(String khid) {
        this.khid = khid;
    }

    public String getRiqi() {
        return this.riqi;
    }
    
    public void setRiqi(String riqi) {
        this.riqi = riqi;
    }

    public String getDizhi() {
        return this.dizhi;
    }
    
    public void setDizhi(String dizhi) {
        this.dizhi = dizhi;
    }

    public String getZhuangtai() {
        return this.zhuangtai;
    }
    
    public void setZhuangtai(String zhuangtai) {
        this.zhuangtai = zhuangtai;
    }
   








}