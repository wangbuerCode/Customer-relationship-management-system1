package com.crm.entity;



/**
 * Dingdanmingxi entity. @author MyEclipse Persistence Tools
 */

public class Dingdanmingxi  implements java.io.Serializable {


    // Fields    

     private Integer dmid;
     private String ddid;
     private String shangpin;
     private String shuliang;
     private String danwei;
     private String danjia;
     private String jine;
     private String khid;

    // Constructors

    /** default constructor */
    public Dingdanmingxi() {
    }

    
    /** full constructor */
    public Dingdanmingxi(String ddid, String shangpin, String shuliang, String danwei, String danjia, String jine, String khid) {
        this.ddid = ddid;
        this.shangpin = shangpin;
        this.shuliang = shuliang;
        this.danwei = danwei;
        this.danjia = danjia;
        this.jine = jine;
        this.khid = khid;
    }

   
    // Property accessors

    public Integer getDmid() {
        return this.dmid;
    }
    
    public void setDmid(Integer dmid) {
        this.dmid = dmid;
    }

    public String getDdid() {
        return this.ddid;
    }
    
    public void setDdid(String ddid) {
        this.ddid = ddid;
    }

    public String getShangpin() {
        return this.shangpin;
    }
    
    public void setShangpin(String shangpin) {
        this.shangpin = shangpin;
    }

    public String getShuliang() {
        return this.shuliang;
    }
    
    public void setShuliang(String shuliang) {
        this.shuliang = shuliang;
    }

    public String getDanwei() {
        return this.danwei;
    }
    
    public void setDanwei(String danwei) {
        this.danwei = danwei;
    }

    public String getDanjia() {
        return this.danjia;
    }
    
    public void setDanjia(String danjia) {
        this.danjia = danjia;
    }

    public String getJine() {
        return this.jine;
    }
    
    public void setJine(String jine) {
        this.jine = jine;
    }
   
    public String getKhid() {
        return this.khid;
    }
    
    public void setKhid(String khid) {
        this.khid = khid;
    }







}