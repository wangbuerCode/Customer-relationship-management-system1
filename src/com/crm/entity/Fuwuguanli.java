package com.crm.entity;

/**
 * Fuwuguanli entity. @author MyEclipse Persistence Tools
 */

public class Fuwuguanli implements java.io.Serializable {

	// Fields

	private Integer fwid;
	private String fwleixing;
	private String fwgaiyao;
	private Integer khid;
	private String fwzhuangtai;
	private String fwqingqiu;
	private String chuangjianrenid;
	private String chuangjianshijian;
	private String id;
	private String chuliren;
	private String fwchuli;
	private String chulishijian;
	private String chulijieguo;
	private String manyidu;
	private String fenpeishijian;

	// Constructors

	/** default constructor */
	public Fuwuguanli() {
	}

	/** full constructor */
	public Fuwuguanli(String fwleixing, String fwgaiyao, Integer khid,
			String fwzhuangtai, String fwqingqiu, String chuangjianrenid,
			String chuangjianshijian, String id, String chuliren,
			String fwchuli, String chulishijian, String chulijieguo,
			String manyidu, String fenpeishijian) {
		this.fwleixing = fwleixing;
		this.fwgaiyao = fwgaiyao;
		this.khid = khid;
		this.fwzhuangtai = fwzhuangtai;
		this.fwqingqiu = fwqingqiu;
		this.chuangjianrenid = chuangjianrenid;
		this.chuangjianshijian = chuangjianshijian;
		this.id = id;
		this.chuliren = chuliren;
		this.fwchuli = fwchuli;
		this.chulishijian = chulishijian;
		this.chulijieguo = chulijieguo;
		this.manyidu = manyidu;
		this.fenpeishijian = fenpeishijian;
	}

	// Property accessors

	public Integer getFwid() {
		return this.fwid;
	}

	public void setFwid(Integer fwid) {
		this.fwid = fwid;
	}

	public String getFwleixing() {
		return this.fwleixing;
	}

	public void setFwleixing(String fwleixing) {
		this.fwleixing = fwleixing;
	}

	public String getFwgaiyao() {
		return this.fwgaiyao;
	}

	public void setFwgaiyao(String fwgaiyao) {
		this.fwgaiyao = fwgaiyao;
	}

	public Integer getKhid() {
		return this.khid;
	}

	public void setKhid(Integer khid) {
		this.khid = khid;
	}

	public String getFwzhuangtai() {
		return this.fwzhuangtai;
	}

	public void setFwzhuangtai(String fwzhuangtai) {
		this.fwzhuangtai = fwzhuangtai;
	}

	public String getFwqingqiu() {
		return this.fwqingqiu;
	}

	public void setFwqingqiu(String fwqingqiu) {
		this.fwqingqiu = fwqingqiu;
	}

	public String getChuangjianrenid() {
		return this.chuangjianrenid;
	}

	public void setChuangjianrenid(String chuangjianrenid) {
		this.chuangjianrenid = chuangjianrenid;
	}

	public String getChuangjianshijian() {
		return this.chuangjianshijian;
	}

	public void setChuangjianshijian(String chuangjianshijian) {
		this.chuangjianshijian = chuangjianshijian;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChuliren() {
		return this.chuliren;
	}

	public void setChuliren(String chuliren) {
		this.chuliren = chuliren;
	}

	public String getFwchuli() {
		return this.fwchuli;
	}

	public void setFwchuli(String fwchuli) {
		this.fwchuli = fwchuli;
	}

	public String getChulishijian() {
		return this.chulishijian;
	}

	public void setChulishijian(String chulishijian) {
		this.chulishijian = chulishijian;
	}

	public String getChulijieguo() {
		return this.chulijieguo;
	}

	public void setChulijieguo(String chulijieguo) {
		this.chulijieguo = chulijieguo;
	}

	public String getManyidu() {
		return this.manyidu;
	}

	public void setManyidu(String manyidu) {
		this.manyidu = manyidu;
	}

	public String getFenpeishijian() {
		return this.fenpeishijian;
	}

	public void setFenpeishijian(String fenpeishijian) {
		this.fenpeishijian = fenpeishijian;
	}

}