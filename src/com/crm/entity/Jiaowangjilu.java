package com.crm.entity;



/**
 * Jiaowangjilu entity. @author MyEclipse Persistence Tools
 */

public class Jiaowangjilu  implements java.io.Serializable {


    // Fields    

     private Integer jwid;
     private Integer khid;
     private String jwshijian;
     private String jwdidian;
     private String gaiyao;
     private String xiangxixinxi;
     private String beizhu;


    // Constructors

    /** default constructor */
    public Jiaowangjilu() {
    }

    
    /** full constructor */
    public Jiaowangjilu(Integer khid, String jwshijian, String jwdidian, String gaiyao, String xiangxixinxi, String beizhu) {
        this.khid = khid;
        this.jwshijian = jwshijian;
        this.jwdidian = jwdidian;
        this.gaiyao = gaiyao;
        this.xiangxixinxi = xiangxixinxi;
        this.beizhu = beizhu;
    }

   
    // Property accessors

    public Integer getJwid() {
        return this.jwid;
    }
    
    public void setJwid(Integer jwid) {
        this.jwid = jwid;
    }

    public Integer getKhid() {
        return this.khid;
    }
    
    public void setKhid(Integer khid) {
        this.khid = khid;
    }

    public String getJwshijian() {
        return this.jwshijian;
    }
    
    public void setJwshijian(String jwshijian) {
        this.jwshijian = jwshijian;
    }

    public String getJwdidian() {
        return this.jwdidian;
    }
    
    public void setJwdidian(String jwdidian) {
        this.jwdidian = jwdidian;
    }

    public String getGaiyao() {
        return this.gaiyao;
    }
    
    public void setGaiyao(String gaiyao) {
        this.gaiyao = gaiyao;
    }

    public String getXiangxixinxi() {
        return this.xiangxixinxi;
    }
    
    public void setXiangxixinxi(String xiangxixinxi) {
        this.xiangxixinxi = xiangxixinxi;
    }

    public String getBeizhu() {
        return this.beizhu;
    }
    
    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }
   








}