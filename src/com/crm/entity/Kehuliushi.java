package com.crm.entity;

/**
 * Kehuliushi entity. @author MyEclipse Persistence Tools
 */

public class Kehuliushi implements java.io.Serializable {

	// Fields

	private Integer lsid;
	private Integer khid;
	private String id;
	private String shangcixiadansj;
	private String quedingliushisj;
	private String zanhuancuoshi;
	private String liushiyuanyin;
	private String liushizhuangtai;
	private Integer bianhao;
	private String cha;
	// Constructors

	/** default constructor */
	public Kehuliushi() {
	}

	/** full constructor */
	public Kehuliushi(Integer khid, String id, String shangcixiadansj,
			String quedingliushisj, String zanhuancuoshi, String liushiyuanyin,
			String liushizhuangtai, Integer bianhao, String cha) {
		this.khid = khid;
		this.id = id;
		this.shangcixiadansj = shangcixiadansj;
		this.quedingliushisj = quedingliushisj;
		this.zanhuancuoshi = zanhuancuoshi;
		this.liushiyuanyin = liushiyuanyin;
		this.liushizhuangtai = liushizhuangtai;
		this.bianhao = bianhao;
		this.cha = cha;
	}

	// Property accessors

	public Integer getLsid() {
		return this.lsid;
	}

	public void setLsid(Integer lsid) {
		this.lsid = lsid;
	}

	public Integer getKhid() {
		return this.khid;
	}

	public void setKhid(Integer khid) {
		this.khid = khid;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getShangcixiadansj() {
		return this.shangcixiadansj;
	}

	public void setShangcixiadansj(String shangcixiadansj) {
		this.shangcixiadansj = shangcixiadansj;
	}

	public String getQuedingliushisj() {
		return this.quedingliushisj;
	}

	public void setQuedingliushisj(String quedingliushisj) {
		this.quedingliushisj = quedingliushisj;
	}

	public String getZanhuancuoshi() {
		return this.zanhuancuoshi;
	}

	public void setZanhuancuoshi(String zanhuancuoshi) {
		this.zanhuancuoshi = zanhuancuoshi;
	}

	public String getLiushiyuanyin() {
		return this.liushiyuanyin;
	}

	public void setLiushiyuanyin(String liushiyuanyin) {
		this.liushiyuanyin = liushiyuanyin;
	}

	public String getLiushizhuangtai() {
		return this.liushizhuangtai;
	}

	public void setLiushizhuangtai(String liushizhuangtai) {
		this.liushizhuangtai = liushizhuangtai;
	}

	public Integer getBianhao() {
		return this.bianhao;
	}

	public void setBianhao(Integer bianhao) {
		this.bianhao = bianhao;
	}
	public String getCha() {
		return this.cha;
	}

	public void setCha(String cha) {
		this.cha = cha;
	}

}