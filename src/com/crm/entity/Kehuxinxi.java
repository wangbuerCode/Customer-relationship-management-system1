package com.crm.entity;

/**
 * Kehuxinxi entity. @author MyEclipse Persistence Tools
 */

public class Kehuxinxi implements java.io.Serializable {

	// Fields

	private Integer khid;
	private String khname;
	private String khdiqu;
	private String khjingli;
	private String khdengji;
	private Integer khmanyidu;
	private Integer khxinyong;
	private String dizhi;
	private Integer youbian;
	private String dianhua;
	private String chuanzhen;
	private String wangzhi;
	private String zhucehao;
	private String faren;
	private String zhucezijin;
	private String nianyingyee;
	private String kaihuyinhang;
	private Long yinhangzhanghao;
	private String dishuidengjihao;
	private String guoshuidengjihao;
	private Integer bianhao;
	private String zonge;

	// Constructors

	/** default constructor */
	public Kehuxinxi() {
	}

	/** full constructor */
	public Kehuxinxi(String khname, String khdiqu, String khjingli,
			String khdengji, Integer khmanyidu, Integer khxinyong,
			String dizhi, Integer youbian, String dianhua, String chuanzhen,
			String wangzhi, String zhucehao, String faren, String zhucezijin,
			String nianyingyee, String kaihuyinhang, Long yinhangzhanghao,
			String dishuidengjihao, String guoshuidengjihao, Integer bianhao,
			String zonge) {
		this.khname = khname;
		this.khdiqu = khdiqu;
		this.khjingli = khjingli;
		this.khdengji = khdengji;
		this.khmanyidu = khmanyidu;
		this.khxinyong = khxinyong;
		this.dizhi = dizhi;
		this.youbian = youbian;
		this.dianhua = dianhua;
		this.chuanzhen = chuanzhen;
		this.wangzhi = wangzhi;
		this.zhucehao = zhucehao;
		this.faren = faren;
		this.zhucezijin = zhucezijin;
		this.nianyingyee = nianyingyee;
		this.kaihuyinhang = kaihuyinhang;
		this.yinhangzhanghao = yinhangzhanghao;
		this.dishuidengjihao = dishuidengjihao;
		this.guoshuidengjihao = guoshuidengjihao;
		this.bianhao = bianhao;
		this.zonge = zonge;
	}

	// Property accessors

	public Integer getKhid() {
		return this.khid;
	}

	public void setKhid(Integer khid) {
		this.khid = khid;
	}

	public String getKhname() {
		return this.khname;
	}

	public void setKhname(String khname) {
		this.khname = khname;
	}

	public String getKhdiqu() {
		return this.khdiqu;
	}

	public void setKhdiqu(String khdiqu) {
		this.khdiqu = khdiqu;
	}

	public String getKhjingli() {
		return this.khjingli;
	}

	public void setKhjingli(String khjingli) {
		this.khjingli = khjingli;
	}

	public String getKhdengji() {
		return this.khdengji;
	}

	public void setKhdengji(String khdengji) {
		this.khdengji = khdengji;
	}

	public Integer getKhmanyidu() {
		return this.khmanyidu;
	}

	public void setKhmanyidu(Integer khmanyidu) {
		this.khmanyidu = khmanyidu;
	}

	public Integer getKhxinyong() {
		return this.khxinyong;
	}

	public void setKhxinyong(Integer khxinyong) {
		this.khxinyong = khxinyong;
	}

	public String getDizhi() {
		return this.dizhi;
	}

	public void setDizhi(String dizhi) {
		this.dizhi = dizhi;
	}

	public Integer getYoubian() {
		return this.youbian;
	}

	public void setYoubian(Integer youbian) {
		this.youbian = youbian;
	}

	public String getDianhua() {
		return this.dianhua;
	}

	public void setDianhua(String dianhua) {
		this.dianhua = dianhua;
	}

	public String getChuanzhen() {
		return this.chuanzhen;
	}

	public void setChuanzhen(String chuanzhen) {
		this.chuanzhen = chuanzhen;
	}

	public String getWangzhi() {
		return this.wangzhi;
	}

	public void setWangzhi(String wangzhi) {
		this.wangzhi = wangzhi;
	}

	public String getZhucehao() {
		return this.zhucehao;
	}

	public void setZhucehao(String zhucehao) {
		this.zhucehao = zhucehao;
	}

	public String getFaren() {
		return this.faren;
	}

	public void setFaren(String faren) {
		this.faren = faren;
	}

	public String getZhucezijin() {
		return this.zhucezijin;
	}

	public void setZhucezijin(String zhucezijin) {
		this.zhucezijin = zhucezijin;
	}

	public String getNianyingyee() {
		return this.nianyingyee;
	}

	public void setNianyingyee(String nianyingyee) {
		this.nianyingyee = nianyingyee;
	}

	public String getKaihuyinhang() {
		return this.kaihuyinhang;
	}

	public void setKaihuyinhang(String kaihuyinhang) {
		this.kaihuyinhang = kaihuyinhang;
	}

	public Long getYinhangzhanghao() {
		return this.yinhangzhanghao;
	}

	public void setYinhangzhanghao(Long yinhangzhanghao) {
		this.yinhangzhanghao = yinhangzhanghao;
	}

	public String getDishuidengjihao() {
		return this.dishuidengjihao;
	}

	public void setDishuidengjihao(String dishuidengjihao) {
		this.dishuidengjihao = dishuidengjihao;
	}

	public String getGuoshuidengjihao() {
		return this.guoshuidengjihao;
	}

	public void setGuoshuidengjihao(String guoshuidengjihao) {
		this.guoshuidengjihao = guoshuidengjihao;
	}

	public Integer getBianhao() {
		return this.bianhao;
	}

	public void setBianhao(Integer bianhao) {
		this.bianhao = bianhao;
	}

	public String getZonge() {
		return this.zonge;
	}

	public void setZonge(String zonge) {
		this.zonge = zonge;
	}

}