package com.crm.entity;

/**
 * Kucun entity. @author MyEclipse Persistence Tools
 */

public class Kucun implements java.io.Serializable {

	// Fields

	private Integer kcid;
	private String kcchanpin;
	private String cangku;
	private String huowei;
	private String jianshu;
	private String beizhu;

	// Constructors

	/** default constructor */
	public Kucun() {
	}

	/** full constructor */
	public Kucun(String kcchanpin, String cangku, String huowei,
			String jianshu, String beizhu) {
		this.kcchanpin = kcchanpin;
		this.cangku = cangku;
		this.huowei = huowei;
		this.jianshu = jianshu;
		this.beizhu = beizhu;
	}

	// Property accessors

	public Integer getKcid() {
		return this.kcid;
	}

	public void setKcid(Integer kcid) {
		this.kcid = kcid;
	}

	public String getKcchanpin() {
		return this.kcchanpin;
	}

	public void setKcchanpin(String kcchanpin) {
		this.kcchanpin = kcchanpin;
	}

	public String getCangku() {
		return this.cangku;
	}

	public void setCangku(String cangku) {
		this.cangku = cangku;
	}

	public String getHuowei() {
		return this.huowei;
	}

	public void setHuowei(String huowei) {
		this.huowei = huowei;
	}

	public String getJianshu() {
		return this.jianshu;
	}

	public void setJianshu(String jianshu) {
		this.jianshu = jianshu;
	}

	public String getBeizhu() {
		return this.beizhu;
	}

	public void setBeizhu(String beizhu) {
		this.beizhu = beizhu;
	}

}