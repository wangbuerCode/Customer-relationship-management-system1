package com.crm.entity;



/**
 * Lianxiren entity. @author MyEclipse Persistence Tools
 */

public class Lianxiren  implements java.io.Serializable {


    // Fields    

     private Integer lid;
     private Integer khid;
     private String name;
     private String sex;
     private String zhiwei;
     private String bangongdh;
     private String dianhua;
     private String beizhu;


    // Constructors

    /** default constructor */
    public Lianxiren() {
    }

    
    /** full constructor */
    public Lianxiren(Integer khid, String name, String sex, String zhiwei, String bangongdh, String dianhua, String beizhu) {
        this.khid = khid;
        this.name = name;
        this.sex = sex;
        this.zhiwei = zhiwei;
        this.bangongdh = bangongdh;
        this.dianhua = dianhua;
        this.beizhu = beizhu;
    }

   
    // Property accessors

    public Integer getLid() {
        return this.lid;
    }
    
    public void setLid(Integer lid) {
        this.lid = lid;
    }

    public Integer getKhid() {
        return this.khid;
    }
    
    public void setKhid(Integer khid) {
        this.khid = khid;
    }

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return this.sex;
    }
    
    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getZhiwei() {
        return this.zhiwei;
    }
    
    public void setZhiwei(String zhiwei) {
        this.zhiwei = zhiwei;
    }

    public String getBangongdh() {
        return this.bangongdh;
    }
    
    public void setBangongdh(String bangongdh) {
        this.bangongdh = bangongdh;
    }

    public String getDianhua() {
        return this.dianhua;
    }
    
    public void setDianhua(String dianhua) {
        this.dianhua = dianhua;
    }

    public String getBeizhu() {
        return this.beizhu;
    }
    
    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }
   








}