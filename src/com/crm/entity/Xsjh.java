package com.crm.entity;



/**
 * Xsjh entity. @author MyEclipse Persistence Tools
 */

public class Xsjh  implements java.io.Serializable {


    // Fields    

     private Integer xid;
     private String jihuilaiyuan;
     private String kehumingcheng;
     private Integer chenggongjilv;
     private String gaiyao;
     private String lianxiren;
     private String lianxirendh;
     private String jihuimiaoshu;
     private String chuangjinren;
     private String chuangjianshijian;
     private String zhipairen;
     private String zhipaishijian;
     private String zhuangtai;


    // Constructors

    /** default constructor */
    public Xsjh() {
    }

    
    /** full constructor */
    public Xsjh(String jihuilaiyuan, String kehumingcheng, Integer chenggongjilv, String gaiyao, String lianxiren, String lianxirendh, String jihuimiaoshu, String chuangjinren, String chuangjianshijian, String zhipairen, String zhipaishijian, String zhuangtai) {
        this.jihuilaiyuan = jihuilaiyuan;
        this.kehumingcheng = kehumingcheng;
        this.chenggongjilv = chenggongjilv;
        this.gaiyao = gaiyao;
        this.lianxiren = lianxiren;
        this.lianxirendh = lianxirendh;
        this.jihuimiaoshu = jihuimiaoshu;
        this.chuangjinren = chuangjinren;
        this.chuangjianshijian = chuangjianshijian;
        this.zhipairen = zhipairen;
        this.zhipaishijian = zhipaishijian;
        this.zhuangtai = zhuangtai;
    }

   
    // Property accessors

    public Integer getXid() {
        return this.xid;
    }
    
    public void setXid(Integer xid) {
        this.xid = xid;
    }

    public String getJihuilaiyuan() {
        return this.jihuilaiyuan;
    }
    
    public void setJihuilaiyuan(String jihuilaiyuan) {
        this.jihuilaiyuan = jihuilaiyuan;
    }

    public String getKehumingcheng() {
        return this.kehumingcheng;
    }
    
    public void setKehumingcheng(String kehumingcheng) {
        this.kehumingcheng = kehumingcheng;
    }

    public Integer getChenggongjilv() {
        return this.chenggongjilv;
    }
    
    public void setChenggongjilv(Integer chenggongjilv) {
        this.chenggongjilv = chenggongjilv;
    }

    public String getGaiyao() {
        return this.gaiyao;
    }
    
    public void setGaiyao(String gaiyao) {
        this.gaiyao = gaiyao;
    }

    public String getLianxiren() {
        return this.lianxiren;
    }
    
    public void setLianxiren(String lianxiren) {
        this.lianxiren = lianxiren;
    }

    public String getLianxirendh() {
        return this.lianxirendh;
    }
    
    public void setLianxirendh(String lianxirendh) {
        this.lianxirendh = lianxirendh;
    }

    public String getJihuimiaoshu() {
        return this.jihuimiaoshu;
    }
    
    public void setJihuimiaoshu(String jihuimiaoshu) {
        this.jihuimiaoshu = jihuimiaoshu;
    }

    public String getChuangjinren() {
        return this.chuangjinren;
    }
    
    public void setChuangjinren(String chuangjinren) {
        this.chuangjinren = chuangjinren;
    }

    public String getChuangjianshijian() {
        return this.chuangjianshijian;
    }
    
    public void setChuangjianshijian(String chuangjianshijian) {
        this.chuangjianshijian = chuangjianshijian;
    }

    public String getZhipairen() {
        return this.zhipairen;
    }
    
    public void setZhipairen(String zhipairen) {
        this.zhipairen = zhipairen;
    }

    public String getZhipaishijian() {
        return this.zhipaishijian;
    }
    
    public void setZhipaishijian(String zhipaishijian) {
        this.zhipaishijian = zhipaishijian;
    }

    public String getZhuangtai() {
        return this.zhuangtai;
    }
    
    public void setZhuangtai(String zhuangtai) {
        this.zhuangtai = zhuangtai;
    }
   








}