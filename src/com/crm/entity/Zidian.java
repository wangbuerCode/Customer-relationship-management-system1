package com.crm.entity;

/**
 * Zidian entity. @author MyEclipse Persistence Tools
 */

public class Zidian implements java.io.Serializable {

	// Fields

	private Integer zdid;
	private String leibie;
	private String tiaomu;
	private String zhi;
	private String tf;

	// Constructors

	/** default constructor */
	public Zidian() {
	}

	/** full constructor */
	public Zidian(String leibie, String tiaomu, String zhi, String tf) {
		this.leibie = leibie;
		this.tiaomu = tiaomu;
		this.zhi = zhi;
		this.tf = tf;
	}

	// Property accessors

	public Integer getZdid() {
		return this.zdid;
	}

	public void setZdid(Integer zdid) {
		this.zdid = zdid;
	}

	public String getLeibie() {
		return this.leibie;
	}

	public void setLeibie(String leibie) {
		this.leibie = leibie;
	}

	public String getTiaomu() {
		return this.tiaomu;
	}

	public void setTiaomu(String tiaomu) {
		this.tiaomu = tiaomu;
	}

	public String getZhi() {
		return this.zhi;
	}

	public void setZhi(String zhi) {
		this.zhi = zhi;
	}

	public String getTf() {
		return this.tf;
	}

	public void setTf(String tf) {
		this.tf = tf;
	}

}